package com.example.coursetracker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.ReminderHolder> {
    private List<Reminder> reminders = new ArrayList<>();
    private OnReminderClickListener listener;

    @NonNull
    @Override
    public ReminderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reminder_cardview, parent, false);
        return new ReminderHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReminderHolder holder, int position) {
        Reminder currentReminder = reminders.get(position);
        holder.textTitle.setText(currentReminder.getTitle());
        holder.textDate.setText(CommonDateFormatter.getInstance().formatWithTime(currentReminder.getDate(), holder.textDate.getContext()));
    }

    @Override
    public int getItemCount() {
        return reminders.size();
    }

    public Reminder getReminderAt(int index){
        return reminders.get(index);
    }

    public void setReminders(List<Reminder> reminders){
        this.reminders = reminders;
        notifyDataSetChanged();
    }

    class ReminderHolder extends RecyclerView.ViewHolder{
        private TextView textTitle;
        private TextView textDate;

        public ReminderHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.reminder_title);
            textDate = itemView.findViewById(R.id.reminder_date);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int index = getAdapterPosition();
                    if(listener != null && index != RecyclerView.NO_POSITION) {
                        listener.onReminderClick(reminders.get(index));
                    }
                }
            });
        }
    }

    public interface OnReminderClickListener {
        void onReminderClick(Reminder reminder);
    }

    public void setOnReminderClickListener(OnReminderClickListener listener){
        this.listener = listener;
    }
}
