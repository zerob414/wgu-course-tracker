package com.example.coursetracker;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface MentorDao {
    @Insert
    long insert(Mentor mentor);

    @Update
    void update(Mentor mentor);

    @Delete
    void delete(Mentor mentor);

    @Query("DELETE FROM tbl_mentors")
    void deleteAllMentors();

    @Query("SELECT * FROM tbl_mentors ORDER BY name ASC")
    LiveData<List<Mentor>> getAllMentors();
}
