package com.example.coursetracker;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.Date;
import java.util.List;

@Dao
public interface ReminderDao {
    @Insert
    long insert(Reminder reminder);

    @Update
    void update(Reminder reminder);

    @Delete
    void delete(Reminder reminder);

    @Query("DELETE FROM tbl_reminders")
    void deleteAllReminders();

    @Query("DELETE FROM tbl_reminders WHERE date < :cutoff")
    void deleteRemindersBefore(Date cutoff);

    @Query("SELECT * FROM tbl_reminders ORDER BY date ASC")
    LiveData<List<Reminder>> getAllReminders();
}
