package com.example.coursetracker;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ViewAddEditReminderActivity extends AppCompatActivity implements ViewReminderFragment.OnEditReminderClickInterface, EditReminderFragment.OnSaveReminderClickInterface, Reminder.ReminderIdUpdatedListener {
    public static final String EXTRA_ID = "com.example.coursetracker.EXTRA_ID";
    public static final String EXTRA_TITLE = "com.example.coursetracker.EXTRA_TITLE";
    public static final String EXTRA_MESSAGE = "com.example.coursetracker.EXTRA_MESSAGE";
    public static final String EXTRA_ENTITY_ID = "com.example.coursetracker.EXTRA_ENTITY_ID";
    public static final String EXTRA_ENTITY_TYPE = "com.example.coursetracker.EXTRA_ENTITY_TYPE";
    public static final String EXTRA_ENTITY_TITLE = "com.example.coursetracker.EXTRA_ENTITY_TITLE";
    public static final String EXTRA_DATE = "com.example.coursetracker.EXTRA_DATE";

    private ReminderViewModel reminderViewModel;
    private TermViewModel termViewModel;
    private CourseViewModel courseViewModel;
    private AssessmentViewModel assessmentViewModel;

    private Bundle extras;
    private Reminder selectedReminder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_reminder);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        reminderViewModel = ViewModelProviders.of(this).get(ReminderViewModel.class);
        Intent intent = getIntent();
        if (intent == null) {
            Toast.makeText(this, "An error occurred, please restart the app", Toast.LENGTH_LONG).show();
        } else {
            extras = intent.getExtras();
            if (extras == null) {
                extras = new Bundle();
            }
            if (extras.getInt(EXTRA_ID) == 0) {
                Reminder reminder = new Reminder();
                reminder.setEntityId(extras.getInt(EXTRA_ENTITY_ID));
                reminder.setEntityType(extras.getString(EXTRA_ENTITY_TYPE));
                reminder.setEntityTitle(extras.getString(EXTRA_ENTITY_TITLE));
                reminder.setTitle(extras.getString(EXTRA_TITLE));
                reminder.setMessage(extras.getString(EXTRA_MESSAGE));
                long dateMillis = extras.getLong(EXTRA_DATE);
                Date date = new Date();
                if (dateMillis > 0) {
                    date.setTime(dateMillis);
                }
                reminder.setDate(date);
                switchToEditReminder(reminder);
            } else {
                LiveData<List<Reminder>> allReminders = reminderViewModel.getAllReminders();
                allReminders.observe(this, reminders -> {
                    for (Reminder reminder : reminders) {
                        if (reminder.getReminderId() == extras.getInt(EXTRA_ID)) {
                            selectedReminder = reminder;
                            switch (selectedReminder.getEntityType()) {
                                case Reminder.TYPE_TERM:
                                    termViewModel = ViewModelProviders.of(this).get(TermViewModel.class);
                                    LiveData<List<Term>> allTerms = termViewModel.getAllTerms();
                                    allTerms.observe(this, terms -> {
                                        for (Term term : terms) {
                                            if (term.getTermId() == selectedReminder.getEntityId()) {
                                                selectedReminder.setEntityTitle(term.getTitle());
                                                switchToViewReminder(selectedReminder);
                                                break;
                                            }
                                        }
                                    });
                                    break;
                                case Reminder.TYPE_COURSE:
                                    courseViewModel = ViewModelProviders.of(this).get(CourseViewModel.class);
                                    LiveData<List<Course>> allCourses = courseViewModel.getAllCourses();
                                    allCourses.observe(this, courses -> {
                                        for (Course course : courses) {
                                            if (course.getCourseId() == selectedReminder.getEntityId()) {
                                                selectedReminder.setEntityTitle(course.getTitle());
                                                switchToViewReminder(selectedReminder);
                                                break;
                                            }
                                        }
                                    });
                                    break;
                                case Reminder.TYPE_ASSESSMENT:
                                    assessmentViewModel = ViewModelProviders.of(this).get(AssessmentViewModel.class);
                                    LiveData<List<Assessment>> allAssessments = assessmentViewModel.getAllAssessments();
                                    allAssessments.observe(this, assessments -> {
                                        for (Assessment assessment : assessments) {
                                            if (assessment.getAssessmentId() == selectedReminder.getEntityId()) {
                                                selectedReminder.setEntityTitle(assessment.getTitle());
                                                switchToViewReminder(selectedReminder);
                                                break;
                                            }
                                        }
                                    });
                                    break;
                            }
                            break;
                        }
                    }
                });
            }

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Fragment fragment = getFragmentManager().findFragmentByTag(EditReminderFragment.TAG);
        if (fragment != null && fragment.isVisible() && selectedReminder != null) {
            switchToViewReminder(selectedReminder);
        } else {
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onEditClick() {
        if (selectedReminder == null) {
            switchToEditReminder(new Reminder());
        } else {
            switchToEditReminder(selectedReminder);
        }
    }

    @Override
    public void onSaveClick(Reminder reminder) {
        if (reminder != null) {
            selectedReminder = reminder;
            if (reminder.getReminderId() == 0) {
                selectedReminder.addIdUpdatedListener(this);
                reminderViewModel.insert(reminder);
            } else {
                reminderViewModel.update(reminder);
                createOrUpdateWorkRequest(reminder);
            }
        }
        switchToViewReminder(selectedReminder);
    }

    /**
     * Adapted from 'Scheduling Notifications on Android with WorkManager' by Adrian Tache at
     * https://medium.com/android-ideas/scheduling-notifications-on-android-with-workmanager-6d84b7f64613
     *
     * @param reminder
     */
    private void createOrUpdateWorkRequest(Reminder reminder) {
        if(reminder != null && reminder.getReminderId() > 0) {
            //we set a tag to be able to cancel all work of this type if needed
            String workTag = "com.example.coursetracker.reminder:" + reminder.getReminderId();

            //cancel any already scheduled notifications for this reminder to prevent duplicates in cases of rescheduling
            WorkManager.getInstance(getApplicationContext()).cancelAllWorkByTag(workTag);

            long delay = calculateDelay(reminder.getDate());

            //store parameters to pass to the PendingIntent and open the appropriate activity on notification click
            Data inputData = new Data.Builder()
                    .putInt(NotificationWorker.NOTIFICATION_REMINDER_ID, reminder.getReminderId())
                    .putString(NotificationWorker.NOTIFICATION_TITLE, reminder.getTitle())
                    .putString(NotificationWorker.NOTIFICATION_MESSAGE, reminder.getMessage())
                    .putString(NotificationWorker.NOTIFICATION_ENTITY_TYPE, reminder.getEntityType())
                    .putInt(NotificationWorker.NOTIFICATION_ENTITY_ID, reminder.getEntityId())
                    .build();

            OneTimeWorkRequest notificationWork = new OneTimeWorkRequest.Builder(NotificationWorker.class)
                    .setInitialDelay(delay, TimeUnit.MILLISECONDS)
                    .setInputData(inputData)
                    .addTag(workTag)
                    .build();

            WorkManager.getInstance(getApplicationContext()).enqueue(notificationWork);
            //alternatively, we can use this form to determine what happens to the existing stack
            // WorkManager.getInstance(context).beginUniqueWork(workTag, ExistingWorkPolicy.REPLACE, notificationWork);
            //ExistingWorkPolicy.REPLACE - Cancel the existing sequence and replace it with the new one
            //ExistingWorkPolicy.KEEP - Keep the existing sequence and ignore your new request
            //ExistingWorkPolicy.APPEND - Append your new sequence to the existing one,
            //running the new sequence's first task after the existing sequence's last task finishes
            String message = "Scheduled notification for " + reminder.getTitle();
            makeToast(message);
        }
        else{
            String message = "Error: Could not schedule notification for " + reminder.getTitle();
            makeToast(message);
        }
    }

    private void makeToast(String msg){
        final String message = msg;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ViewAddEditReminderActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private long calculateDelay(Date date) {
        Date now = new Date();
        if(date.after(now)){
            return date.getTime() - now.getTime();
        }
        return 0;
    }

    @Override
    public void onUpClick() {
        onBackPressed();
    }

    @Override
    public void onHomeClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragmentManager().findFragmentByTag(EditReminderFragment.TAG);
        if (fragment != null && fragment.isVisible() && selectedReminder != null) {
            switchToViewReminder(selectedReminder);
        } else {
            finish();
        }
    }

    private void switchToEditReminder(Reminder reminder) {
        EditReminderFragment newFragment = new EditReminderFragment();
        newFragment.setReminder(reminder);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment, EditReminderFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void switchToViewReminder(Reminder reminder) {
        ViewReminderFragment newFragment = new ViewReminderFragment();
        newFragment.setReminder(reminder);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment, ViewReminderFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void reminderIdUpdated(Reminder reminder) {
        createOrUpdateWorkRequest(reminder);
    }
}
