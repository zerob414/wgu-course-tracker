package com.example.coursetracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.List;

public class ViewAddEditMentorActivity extends AppCompatActivity implements ViewMentorFragment.MentorActionListener, EditMentorFragment.OnSaveMentorClickInterface {
    public static final String EXTRA_ID = "com.example.coursetracker.EXTRA_ID";
    public static final String EXTRA_NAME = "com.example.coursetracker.EXTRA_NAME";
    public static final String EXTRA_PHONE = "com.example.coursetracker.EXTRA_PHONE";
    public static final String EXTRA_EMAIL = "com.example.coursetracker.EXTRA_EMAIL";

    private MentorViewModel mentorViewModel;
    private Bundle extras;
    private Mentor selectedMentor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_mentor);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mentorViewModel = ViewModelProviders.of(this).get(MentorViewModel.class);
        Intent intent = getIntent();
        if(intent == null){
            Toast.makeText(this, "An error occurred, please restart the app", Toast.LENGTH_LONG).show();
        }
        else{
            extras = intent.getExtras();
            if(extras == null){
                extras = new Bundle();
            }
            if(extras.getInt(EXTRA_ID) == 0){
                switchToEditMentor(new Mentor());
            }
            else {
                LiveData<List<Mentor>> allMentors = mentorViewModel.getAllMentors();
                allMentors.observe(this, mentors -> {
                    for(Mentor mentor : mentors){
                        if(mentor.getMentorId() == extras.getInt(EXTRA_ID)) {
                            selectedMentor = mentor;
                            switchToViewMentor(selectedMentor);
                            break;
                        }
                    }
                });
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Fragment fragment = getFragmentManager().findFragmentByTag(EditMentorFragment.TAG);
        if (fragment != null && fragment.isVisible() && selectedMentor != null) {
            switchToViewMentor(selectedMentor);
        }
        else {
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void share(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String mentorInfo = selectedMentor.getName() + "\n" + selectedMentor.getPhone() + "\n" + selectedMentor.getEmail();
        sendIntent.putExtra(Intent.EXTRA_TEXT, mentorInfo);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Here's contact info for " + selectedMentor.getName());
        sendIntent.setType("text/plain");
        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }

    @Override
    public void onEditClick() {
        if(selectedMentor == null){
            switchToEditMentor(new Mentor());
        }
        else{
            switchToEditMentor(selectedMentor);
        }
    }

    @Override
    public void onSaveClick(Mentor mentor) {
        if(mentor != null){
            selectedMentor = mentor;
            if(mentor.getMentorId() == 0){
                mentorViewModel.insert(mentor);
            }
            else {
                mentorViewModel.update(mentor);
            }
        }
        switchToViewMentor(selectedMentor);
    }

    @Override
    public void onUpClick() {
        onBackPressed();
    }

    @Override
    public void onHomeClick(){
        onBackPressed();
    }

    @Override
    public void onBackPressed(){
        Fragment fragment = getFragmentManager().findFragmentByTag(EditMentorFragment.TAG);
        if (fragment != null && fragment.isVisible() && selectedMentor != null) {
            switchToViewMentor(selectedMentor);
        }
        else {
           finish();
        }
    }

    private void switchToEditMentor(Mentor mentor) {
        EditMentorFragment newFragment = new EditMentorFragment();
        newFragment.setMentor(mentor);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment, EditMentorFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void switchToViewMentor(Mentor mentor) {
        ViewMentorFragment newFragment = new ViewMentorFragment();
        newFragment.setMentor(mentor);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment, ViewMentorFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void onButtonPhoneClick(View view){
        call();
    }

    public void onButtonEmailClick(View view){
        email();
    }

    @Override
    public void call() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + extras.getString(EXTRA_PHONE)));
        startActivity(intent);
    }

    @Override
    public void email() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        String[] addresses = {extras.getString(EXTRA_EMAIL)};
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
