package com.example.coursetracker;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

/**
 * Class for scheduling notifcations.
 *
 * Adapted from 'Scheduling Notifications on Android with WorkManager' by Adrian Tache at
 * https://medium.com/android-ideas/scheduling-notifications-on-android-with-workmanager-6d84b7f64613
 */

public class NotificationWorker extends Worker {
    private static final String NOTIFICATION_CHANNEL = R.string.app_name + " Reminder";

    public static final String NOTIFICATION_REMINDER_ID = "com.example.coursetracker.REMINDER_ID";
    public static final String NOTIFICATION_TITLE = "com.example.coursetracker.TITLE";
    public static final String NOTIFICATION_MESSAGE = "com.example.coursetracker.MESSAGE";
    public static final String NOTIFICATION_ENTITY_TYPE = "com.example.coursetracker.ENTITY_TYPE";
    public static final String NOTIFICATION_ENTITY_ID = "com.example.coursetracker.ENTITY_ID";

    public NotificationWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    @NonNull
    @Override
    public Result doWork() {
        // Method to trigger an instant notification
        triggerNotification();

        return Result.success();
        // (Returning RETRY tells WorkManager to try this task again
        // later; FAILURE says not to try again.)
    }

    /**
     * Adapted from 'Building Notifications on Android O+' by Adrian Tache at
     * https://medium.com/android-ideas/building-notifications-on-android-o-ae36006a921b
     */
    public void triggerNotification(){
        Data inputData = getInputData();
        int reminderId = inputData.getInt(NOTIFICATION_REMINDER_ID, 0);
        String title = inputData.getString(NOTIFICATION_TITLE);
        String message = inputData.getString(NOTIFICATION_MESSAGE);
        String entityType = inputData.getString(NOTIFICATION_ENTITY_TYPE);
        int entityId = inputData.getInt(NOTIFICATION_ENTITY_ID, 0);

        if(reminderId > 0 &&
                title != null &&
                !title.trim().isEmpty() &&
                message != null &&
                !message.trim().isEmpty() &&
                entityType != null &&
                !entityType.trim().isEmpty() &&
                entityId > 0
        ) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //define the importance level of the notification
                int importance = NotificationManager.IMPORTANCE_DEFAULT;

                //build the actual notification channel, giving it a unique ID and name
                NotificationChannel channel =
                        new NotificationChannel(NOTIFICATION_CHANNEL, NOTIFICATION_CHANNEL, importance);

                //we can optionally add a description for the channel
                String description = "A channel which shows " + R.string.app_name + " reminders";
                channel.setDescription(description);

                //we can optionally set notification LED colour
                channel.setLightColor(Color.BLUE);

                // Register the channel with the system
                NotificationManager notificationManager = (NotificationManager) getApplicationContext().
                        getSystemService(Context.NOTIFICATION_SERVICE);
                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(channel);
                }
            }
            //create an intent to open the event details activity
            Intent intent = null;
            switch(entityType) {
                case Reminder.TYPE_TERM:
                    intent = new Intent(getApplicationContext(), ViewAddEditTermActivity.class);
                    intent.putExtra(ViewAddEditTermActivity.EXTRA_ID, entityId);
                    break;
                case Reminder.TYPE_COURSE:
                    intent = new Intent(getApplicationContext(), ViewAddEditCourseActivity.class);
                    intent.putExtra(ViewAddEditCourseActivity.EXTRA_ID, entityId);
                    break;
                case Reminder.TYPE_ASSESSMENT:
                    intent = new Intent(getApplicationContext(), ViewAddEditAssessmentActivity.class);
                    intent.putExtra(ViewAddEditAssessmentActivity.EXTRA_ID, entityId);
                    break;
            }
            if(intent != null) {
                //put together the PendingIntent
                PendingIntent pendingIntent =
                        PendingIntent.getActivity(getApplicationContext(), 1, intent, FLAG_UPDATE_CURRENT);
                //build the notification
                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL)
                                .setSmallIcon(R.drawable.ic_notification)
                                .setContentTitle(title)
                                .setContentText(message)
                                .setContentIntent(pendingIntent)
                                .setAutoCancel(true)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                //trigger the notification
                NotificationManagerCompat notificationManager =
                        NotificationManagerCompat.from(getApplicationContext());

                //we give each notification the ID of the event it's describing,
                //to ensure they all show up and there are no duplicates
                notificationManager.notify(reminderId, notificationBuilder.build());
            }
        }
    }
}
