package com.example.coursetracker;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "tbl_assessments")
public class Assessment {
    @Ignore
    public static final String OBJECTIVE = "Objective";
    @Ignore
    public static final String PERFORMANCE = "Performance";
    @Ignore
    public static final int NO_SCORE = -1;
    @Ignore
    public static final int NO_COURSE = -1;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "assessment_id")
    private int assessmentId;
    private String title;
    private String type;
    @ColumnInfo(name = "start_date")
    private Date startDate;
    @ColumnInfo(name = "due_date")
    private Date dueDate;
    private String notes;
    private int score;
    @ColumnInfo(name = "course_id")
    private int courseId;

    public Assessment(String title, String type,Date startDate, Date dueDate, String notes, int score, Integer courseId) {
        this.title = title;
        this.type = type;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.notes = notes;
        this.score = score;
        this.courseId = courseId;
    }

    @Ignore
    public Assessment(){
        startDate = new Date();
        dueDate = new Date();
    }

    public int getAssessmentId() {
        return assessmentId;
    }

    public void setAssessmentId(int assessmentId) {
        this.assessmentId = assessmentId;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public String getNotes() {
        return notes;
    }

    public int getScore() {
        return score;
    }

    public int getCourseId() { return courseId; }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }
}
