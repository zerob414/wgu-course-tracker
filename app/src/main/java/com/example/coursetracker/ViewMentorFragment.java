package com.example.coursetracker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MentorActionListener} interface
 * to handle interaction events.
 * Use the {@link ViewMentorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewMentorFragment extends Fragment {
    public static final String TAG = "com.example.coursetracker.ViewMentorFragment";

    private TextView textName;
    private Button buttonPhone;
    private Button buttonEmail;

    private Mentor selectedMentor;

    private CourseViewModel courseViewModel;

    private MentorActionListener mListener;

    public ViewMentorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id    The mentor's SQLite/Room ID number
     * @param name  The mentor's name
     * @param phone The mentor's phone number
     * @param email The mentor's email address
     * @return A new instance of fragment ViewMentorFragment.
     */
    public static ViewMentorFragment newInstance(int id, String name, String phone, String email) {
        ViewMentorFragment fragment = new ViewMentorFragment();
        Bundle args = new Bundle();
        args.putInt(ViewAddEditMentorActivity.EXTRA_ID, id);
        args.putString(ViewAddEditMentorActivity.EXTRA_NAME, name);
        args.putString(ViewAddEditMentorActivity.EXTRA_PHONE, phone);
        args.putString(ViewAddEditMentorActivity.EXTRA_EMAIL, email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        textName = getView().findViewById(R.id.textTitle);
        buttonPhone = getView().findViewById(R.id.buttonPhone);
        buttonEmail = getView().findViewById(R.id.buttonEmail);

        getActivity().setTitle("View Mentor");

        if(selectedMentor != null){
            textName.setText(selectedMentor.getName());
            buttonPhone.setText(selectedMentor.getPhone());
            buttonEmail.setText(selectedMentor.getEmail());

            RecyclerView recyclerView = getView().findViewById(R.id.recyclerCourses);
            recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
            recyclerView.setHasFixedSize(true);

            CourseFlatAdapter adapter = new CourseFlatAdapter();
            recyclerView.setAdapter(adapter);

            courseViewModel = ViewModelProviders.of(this).get(CourseViewModel.class);
            courseViewModel.getAllCourses().observe(this, new Observer<List<Course>>() {
                @Override
                public void onChanged(List<Course> courses) {
                    ArrayList<Course> filteredCourseList = new ArrayList<>();
                    for(Course course : courses){
                        if(course.getMentorId() == selectedMentor.getMentorId()){
                            filteredCourseList.add(course);
                        }
                    }
                    adapter.setCourses(filteredCourseList);
                }
            });
            adapter.setOnCourseClickListener(new CourseFlatAdapter.OnCourseClickListener() {
                @Override
                public void onCourseClick(Course course) {
                    Intent intent = new Intent(ViewMentorFragment.this.getActivity(), ViewAddEditCourseActivity.class);
                    intent.putExtra(ViewAddEditCourseActivity.EXTRA_ID, course.getCourseId());
                    startActivity(intent);
                }
            });
        }
    }

    public void setMentor(Mentor mentor){
        selectedMentor = mentor;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menuInflater.inflate(R.menu.view_mentor_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.edit_mentor:
                mListener.onEditClick();
                return true;
            case R.id.share:
                mListener.share();
                return true;
            case android.R.id.home:
                mListener.onHomeClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_mentor, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MentorActionListener) {
            mListener = (MentorActionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MentorActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Interface to send events back to the Activity.
     */
    public interface MentorActionListener {
        void onEditClick();
        void onHomeClick();
        void share();
        void call();
        void email();
    }
}
