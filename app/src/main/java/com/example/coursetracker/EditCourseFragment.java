package com.example.coursetracker;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnSaveCourseClickInterface} interface
 * to handle interaction events.
 * Use the {@link EditCourseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditCourseFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener {
    public static final String TAG = "com.example.coursetracker.EditCourseFragment";

    private EditText titleText;
    private Spinner termSpinner;
    private Spinner statusSpinner;
    private Button startDateButton;
    private Button endDateButton;
    private EditText notesText;
    private Spinner mentorSpinner;

    private Course selectedCourse;

    private OnSaveCourseClickInterface mListener;

    public EditCourseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id    The course's SQLite/Room ID number, null if new Course
     * @param title  The course's title
     * @param status The course's status
     * @param startDate The course's start date
     * @param endDate The course's end date
     * @param notes Notes for the course
     * @param mentorId Foreign key to this course's mentor
     * @param termId Foreign key to this course's term
     * @return A new instance of fragment EditCourseFragment.
     */
    public static EditCourseFragment newInstance(int id, String title, String status, Date startDate, Date endDate, String notes, int mentorId, int termId) {
        EditCourseFragment fragment = new EditCourseFragment();
        Bundle args = new Bundle();
        args.putInt(ViewAddEditCourseActivity.EXTRA_ID, id);
        args.putString(ViewAddEditCourseActivity.EXTRA_TITLE, title);
        args.putString(ViewAddEditCourseActivity.EXTRA_STATUS, status);
        args.putLong(ViewAddEditCourseActivity.EXTRA_START_DATE, startDate.getTime());
        args.putLong(ViewAddEditCourseActivity.EXTRA_END_DATE, endDate.getTime());
        args.putString(ViewAddEditCourseActivity.EXTRA_NOTES, notes);
        args.putInt(ViewAddEditCourseActivity.EXTRA_MENTOR_ID, mentorId);
        args.putInt(ViewAddEditCourseActivity.EXTRA_TERM_ID, termId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        titleText = getView().findViewById(R.id.textTitle);
        termSpinner = getView().findViewById(R.id.spinnerTerm);
        statusSpinner = getView().findViewById(R.id.spinnerStatus);
        startDateButton = getView().findViewById(R.id.buttonStartDate);
        endDateButton = getView().findViewById(R.id.buttonEndDate);
        notesText = getView().findViewById(R.id.textNotes);
        mentorSpinner = getView().findViewById(R.id.spinnerMentor);

        titleText.setOnFocusChangeListener(this);
        notesText.setOnFocusChangeListener(this);

        if(selectedCourse != null) {
            titleText.setText(selectedCourse.getTitle());
            startDateButton.setText(CommonDateFormatter.getInstance().format(selectedCourse.getStartDate()));
            endDateButton.setText(CommonDateFormatter.getInstance().format(selectedCourse.getEndDate()));
            notesText.setText(selectedCourse.getNotes());

            if(selectedCourse.getStatus() != null) {
                for (int i = 0; i < Course.STATUSES_ARRAY.length; i++) {
                    if (selectedCourse.getStatus().equals(Course.STATUSES_ARRAY[i])) {
                        statusSpinner.setSelection(i);
                        break;
                    }
                }
            }

            TermViewModel termViewModel = ViewModelProviders.of(this).get(TermViewModel.class);
            LiveData<List<Term>> termList = termViewModel.getAllTerms();
            ArrayAdapter<Term> termArrayAdapter = new ArrayAdapter<Term>(this.getContext(), android.R.layout.simple_spinner_item);
            termList.observe(this, terms -> {
                int selectedIndex = 0;
                for (Term term : terms) {
                    termArrayAdapter.add(term);
                    if (term.getTermId() == selectedCourse.getTermId()) {
                        selectedIndex = terms.indexOf(term);
                        break;
                    }
                }
                termSpinner.setSelection(selectedIndex);
            });
            termSpinner.setAdapter(termArrayAdapter);

            MentorViewModel mentorViewModel = ViewModelProviders.of(this).get(MentorViewModel.class);
            LiveData<List<Mentor>> mentorList = mentorViewModel.getAllMentors();
            ArrayAdapter<Mentor> mentorArrayAdapter = new ArrayAdapter<Mentor>(this.getContext(), android.R.layout.simple_spinner_item);
            mentorList.observe(this, mentors -> {
                int selectedIndex = 0;
                for (Mentor mentor : mentors) {
                    mentorArrayAdapter.add(mentor);
                    if (mentor.getMentorId() == selectedCourse.getMentorId()) {
                        selectedIndex = mentors.indexOf(mentor);
                        break;
                    }
                }
                mentorSpinner.setSelection(selectedIndex);
            });
            mentorSpinner.setAdapter(mentorArrayAdapter);
        }

        startDateButton.setOnClickListener(this);
        endDateButton.setOnClickListener(this);
    }

    public void setCourse(Course course){
        selectedCourse = course;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menuInflater.inflate(R.menu.edit_course_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        //Toast.makeText(this.getContext(), "OptiosnItem Up pressed", Toast.LENGTH_SHORT).show();
        switch (item.getItemId()){
            case R.id.save_course:
                saveCourse();
                return true;
            case android.R.id.home:
                mListener.onUpClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveCourse(){
        if(titleText.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please enter a title", Toast.LENGTH_SHORT).show();
        }
        else if(startDateButton.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please select a start date", Toast.LENGTH_SHORT).show();
        }
        else if(endDateButton.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please select an end date", Toast.LENGTH_SHORT).show();
        }
        else {
            if(selectedCourse == null)selectedCourse = new Course();
            selectedCourse.setTitle(titleText.getText().toString());
            selectedCourse.setStatus(statusSpinner.getSelectedItem().toString());
            selectedCourse.setStartDate(CommonDateFormatter.parse(startDateButton.getText().toString()));
            selectedCourse.setEndDate(CommonDateFormatter.parse(endDateButton.getText().toString()));
            selectedCourse.setNotes(notesText.getText().toString());

            Mentor mentor = (Mentor)mentorSpinner.getSelectedItem();
            selectedCourse.setMentorId(mentor == null ? -1 : mentor.getMentorId());
            Term term = (Term)termSpinner.getSelectedItem();
            selectedCourse.setTermId(term == null ? -1 : term.getTermId());
            mListener.onSaveClick(selectedCourse);
        }
    }

    @Override
    public void onClick(View view) {
        if(view == startDateButton){
            Date courseStartDate = CommonDateFormatter.parse(startDateButton.getText().toString());
            if(courseStartDate == null){
                courseStartDate = new Date();
            }
            Calendar cal = new GregorianCalendar();
            cal.setTime(courseStartDate);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            Calendar cal = new GregorianCalendar();
                            cal.set(Calendar.YEAR, year);
                            cal.set(Calendar.MONTH, monthOfYear);
                            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            startDateButton.setText(CommonDateFormatter.getInstance().format(cal.getTime()));
                        }
                    }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
        else if(view == endDateButton){
            Date courseEndDate = CommonDateFormatter.parse(endDateButton.getText().toString());
            if(courseEndDate == null){
                courseEndDate = new Date();
            }
            Calendar cal = new GregorianCalendar();
            cal.setTime(courseEndDate);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar cal = new GregorianCalendar();
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, monthOfYear);
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    endDateButton.setText(CommonDateFormatter.getInstance().format(cal.getTime()));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_course, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSaveCourseClickInterface) {
            mListener = (OnSaveCourseClickInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement CourseActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(view instanceof EditText && !hasFocus){
            InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Interface to send events back to the Activity.

     */
    public interface OnSaveCourseClickInterface {
        void onSaveClick(Course course);
        void onUpClick();
    }
}
