package com.example.coursetracker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CourseActionListener} interface
 * to handle interaction events.
 * Use the {@link ViewCourseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewCourseFragment extends Fragment {
    public static final String TAG = "com.example.coursetracker.ViewCourseFragment";

    private TextView titleText;
    private TextView termText;
    private TextView statusText;
    private TextView startDateText;
    private TextView endDateText;
    private TextView notesText;
    private TextView mentorText;
    private ImageButton phoneButton;
    private ImageButton emailButton;

    private Course selectedCourse;
    private Mentor selectedCourseMentor;

    private AssessmentViewModel assessmentViewModel;

    private CourseActionListener mListener;

    public ViewCourseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id    The course's SQLite/Room ID number, null if new Course
     * @param title  The course's title
     * @param status The course's status
     * @param startDate The course's start date
     * @param endDate The course's end date
     * @param notes Notes for the course
     * @param mentorId Foreign key to this course's mentor
     * @param termId Foreign key to this course's term
     * @return A new instance of fragment ViewCourseFragment.
     */
    public static ViewCourseFragment newInstance(int id, String title, String status, Date startDate, Date endDate, String notes, int mentorId, int termId) {
        ViewCourseFragment fragment = new ViewCourseFragment();
        Bundle args = new Bundle();
        args.putInt(ViewAddEditCourseActivity.EXTRA_ID, id);
        args.putString(ViewAddEditCourseActivity.EXTRA_TITLE, title);
        args.putString(ViewAddEditCourseActivity.EXTRA_STATUS, status);
        args.putLong(ViewAddEditCourseActivity.EXTRA_START_DATE, startDate.getTime());
        args.putLong(ViewAddEditCourseActivity.EXTRA_END_DATE, endDate.getTime());
        args.putString(ViewAddEditCourseActivity.EXTRA_NOTES, notes);
        args.putInt(ViewAddEditCourseActivity.EXTRA_MENTOR_ID, mentorId);
        args.putInt(ViewAddEditCourseActivity.EXTRA_TERM_ID, termId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        titleText = getView().findViewById(R.id.textTitle);
        termText = getView().findViewById(R.id.textTerm);
        statusText = getView().findViewById(R.id.textStatus);
        startDateText = getView().findViewById(R.id.textStartDate);
        endDateText = getView().findViewById(R.id.textEndDate);
        notesText = getView().findViewById(R.id.textNotes);
        mentorText = getView().findViewById(R.id.textMentor);
        phoneButton = getView().findViewById(R.id.buttonCallMentor);
        emailButton = getView().findViewById(R.id.buttonEmailMentor);

        getActivity().setTitle("View Course");

        if(selectedCourse != null) {
            titleText.setText(selectedCourse.getTitle());
            statusText.setText(selectedCourse.getStatus());
            startDateText.setText(CommonDateFormatter.getInstance().format(selectedCourse.getStartDate()));
            endDateText.setText(CommonDateFormatter.getInstance().format(selectedCourse.getEndDate()));
            notesText.setText(selectedCourse.getNotes());

            mentorText.setText("");
            MentorViewModel mentorViewModel = ViewModelProviders.of(this).get(MentorViewModel.class);
            LiveData<List<Mentor>> allMentors = mentorViewModel.getAllMentors();
            allMentors.observe(this, mentors -> {
                for (Mentor mentor : mentors) {
                    if (mentor.getMentorId() == selectedCourse.getMentorId()) {
                        selectedCourseMentor = mentor;
                        mentorText.setText(mentor.getName());
                        break;
                    }
                }
                if(selectedCourseMentor == null){
                    phoneButton.setVisibility(View.INVISIBLE);
                    emailButton.setVisibility(View.INVISIBLE);
                }
                else{
                    phoneButton.setVisibility(View.VISIBLE);
                    emailButton.setVisibility(View.VISIBLE);
                }
            });

            termText.setText("");
            TermViewModel termViewModel = ViewModelProviders.of(this).get(TermViewModel.class);
            LiveData<List<Term>> allTerms = termViewModel.getAllTerms();
            allTerms.observe(this, terms -> {
                for (Term term : terms) {
                    if (term.getTermId() == selectedCourse.getTermId()) {
                        termText.setText(term.getNumber() + " - " + term.getTitle());
                        break;
                    }
                }
            });

            RecyclerView recyclerView = getView().findViewById(R.id.recyclerAssessments);
            recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
            recyclerView.setHasFixedSize(true);

            AssessmentFlatAdapter adapter = new AssessmentFlatAdapter();
            recyclerView.setAdapter(adapter);

            assessmentViewModel = ViewModelProviders.of(this).get(AssessmentViewModel.class);
            assessmentViewModel.getAllAssessments().observe(this, new Observer<List<Assessment>>() {
                @Override
                public void onChanged(List<Assessment> assessments) {
                    ArrayList<Assessment> filteredAssessmentList = new ArrayList<>();
                    for(Assessment assessment : assessments){
                        if(assessment.getAssessmentId() == selectedCourse.getTermId()){
                            filteredAssessmentList.add(assessment);
                        }
                    }
                    adapter.setAssessments(filteredAssessmentList);
                }
            });
            adapter.setOnAssessmentClickListener(new AssessmentFlatAdapter.OnAssessmentClickListener() {
                @Override
                public void onAssessmentClick(Assessment assessment) {
                    Intent intent = new Intent(ViewCourseFragment.this.getActivity(), ViewAddEditAssessmentActivity.class);
                    intent.putExtra(ViewAddEditAssessmentActivity.EXTRA_ID, assessment.getAssessmentId());
                    startActivity(intent);
                }
            });
        }
    }

    public void setCourse(Course course){
        selectedCourse = course;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menuInflater.inflate(R.menu.view_course_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.add_course_reminder:
                mListener.onAddReminderClick();
                return true;
            case R.id.share:
                mListener.share();
                return true;
            case R.id.edit_course:
                mListener.onEditClick();
                return true;
            case android.R.id.home:
                mListener.onHomeClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_course, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CourseActionListener) {
            mListener = (CourseActionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement CourseActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public Mentor getSelectedCourseMentor(){
        return selectedCourseMentor;
    }

    /**
     * Interface to send events back to the Activity.
     */
    public interface CourseActionListener {
        void onAddReminderClick();
        void onEditClick();
        void onHomeClick();
        void share();
        void call();
        void email();
    }
}
