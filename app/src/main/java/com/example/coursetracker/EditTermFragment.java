package com.example.coursetracker;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnSaveTermClickInterface} interface
 * to handle interaction events.
 * Use the {@link EditTermFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditTermFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener {
    public static final String TAG = "com.example.coursetracker.EditTermFragment";

    private EditText titleText;
    private Button startDateButton;
    private Button endDateButton;
    private NumberPicker termNumberPicker;
    private EditText notesText;

    private Term selectedTerm;

    private OnSaveTermClickInterface mListener;

    public EditTermFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id    The term's SQLite/Room ID number, null if new Term
     * @param title  The term's title
     * @param startDate The term's start date
     * @param endDate The term's end date
     * @param order The term's number for sorting
     * @param notes The term's notes
     * @return A new instance of fragment EditTermFragment.
     */
    public static EditTermFragment newInstance(Integer id, String title, Date startDate, Date endDate, int order, String notes) {
        EditTermFragment fragment = new EditTermFragment();
        Bundle args = new Bundle();
        args.putInt(ViewAddEditTermActivity.EXTRA_ID, id);
        args.putString(ViewAddEditTermActivity.EXTRA_TITLE, title);
        args.putLong(ViewAddEditTermActivity.EXTRA_START_DATE, startDate.getTime());
        args.putLong(ViewAddEditTermActivity.EXTRA_END_DATE, endDate.getTime());
        args.putInt(ViewAddEditTermActivity.EXTRA_NUMBER, order);
        args.putString(ViewAddEditTermActivity.EXTRA_NOTES, notes);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        titleText = getView().findViewById(R.id.textTitle);
        startDateButton = getView().findViewById(R.id.buttonStartDate);
        endDateButton = getView().findViewById(R.id.buttonEndDate);
        termNumberPicker = getView().findViewById(R.id.pickerTermNumber);
        notesText = getView().findViewById(R.id.textNotes);

        titleText.setOnFocusChangeListener(this);

        TermViewModel termViewModel = ViewModelProviders.of(this).get(TermViewModel.class);
        termViewModel.getAllTerms().observe(this, new Observer<List<Term>>() {
            @Override
            public void onChanged(List<Term> terms) {
                int termCount = 1;
                int termSel = 1;
                if(terms.size() > 0) {
                    termCount = terms.size();
                    termSel = termCount + 1;
                }
                termNumberPicker.setMinValue(1);
                termNumberPicker.setMaxValue(termCount + 1);
                if(selectedTerm != null && selectedTerm.getNumber() > 0){
                    termNumberPicker.setValue(selectedTerm.getNumber());
                }
                else {
                    termNumberPicker.setValue(termSel);
                }
            }
        });

        getActivity().setTitle("Add Term");
        if(selectedTerm != null) {
            if (selectedTerm.getTermId() > 0) {
                getActivity().setTitle("Edit Term");
            }
            titleText.setText(selectedTerm.getTitle());
            startDateButton.setText(CommonDateFormatter.getInstance().format(selectedTerm.getStart()));
            endDateButton.setText(CommonDateFormatter.getInstance().format(selectedTerm.getEnd()));
            termNumberPicker.setValue(selectedTerm.getNumber());
            notesText.setText(selectedTerm.getNotes());
        }

        startDateButton.setOnClickListener(this);
        endDateButton.setOnClickListener(this);
    }

    public void setTerm(Term term){
        selectedTerm = term;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menuInflater.inflate(R.menu.edit_term_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.save_term:
                saveTerm();
                return true;
            case android.R.id.home:
                mListener.onUpClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveTerm(){
        if(titleText.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please enter a name", Toast.LENGTH_SHORT).show();
        }
        else if(startDateButton.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please enter a phone number", Toast.LENGTH_SHORT).show();
        }
        else if(endDateButton.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please enter an email address", Toast.LENGTH_SHORT).show();
        }
        else {
            if(selectedTerm == null)selectedTerm = new Term();
            selectedTerm.setTitle(titleText.getText().toString());
            Date start = CommonDateFormatter.parse(startDateButton.getText().toString());
            selectedTerm.setStart(start);
            Date end = CommonDateFormatter.parse(endDateButton.getText().toString());
            selectedTerm.setEnd(end);
            selectedTerm.setNumber(termNumberPicker.getValue());
            selectedTerm.setNotes(notesText.getText().toString());
            mListener.onSaveClick(selectedTerm);
        }
    }

    @Override
    public void onClick(View view) {
        if(view == startDateButton){
            Date termStartDate = CommonDateFormatter.parse(startDateButton.getText().toString());
            if(termStartDate == null){
                termStartDate = new Date();
            }
            Calendar cal = new GregorianCalendar();
            cal.setTime(termStartDate);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar cal = new GregorianCalendar();
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, monthOfYear);
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    startDateButton.setText(CommonDateFormatter.getInstance().format(cal.getTime()));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
        else if(view == endDateButton){
            Date termEndDate = CommonDateFormatter.parse(endDateButton.getText().toString());
            if(termEndDate == null){
                termEndDate = new Date();
            }
            Calendar cal = new GregorianCalendar();
            cal.setTime(termEndDate);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar cal = new GregorianCalendar();
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, monthOfYear);
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    endDateButton.setText(CommonDateFormatter.getInstance().format(cal.getTime()));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_term, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSaveTermClickInterface) {
            mListener = (OnSaveTermClickInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TermActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(view instanceof EditText && !hasFocus){
            InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Interface to send events back to the Activity.

     */
    public interface OnSaveTermClickInterface {
        void onSaveClick(Term term);
        void onUpClick();
    }
}
