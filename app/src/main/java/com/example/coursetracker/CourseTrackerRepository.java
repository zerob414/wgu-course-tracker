package com.example.coursetracker;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class CourseTrackerRepository {
    private TermDao termDao;
    private LiveData<List<Term>> allTerms;

    private CourseDao courseDao;
    private LiveData<List<Course>> allCourses;
    private List<Course> allCoursesList;

    private AssessmentDao assessmentDao;
    private LiveData<List<Assessment>> allAssessments;

    private MentorDao mentorDao;
    private LiveData<List<Mentor>> allMentors;

    private ReminderDao reminderDao;
    private LiveData<List<Reminder>> allReminders;

    public CourseTrackerRepository(Application application){
        CourseTrackerDatabase database = CourseTrackerDatabase.getInstance(application);
        termDao = database.termDao();
        allTerms = termDao.getAllTerms();

        courseDao = database.courseDao();
        allCourses = courseDao.getAllCourses();

        assessmentDao = database.assessmentDao();
        allAssessments = assessmentDao.getAllAssessments();

        mentorDao = database.mentorDao();
        allMentors = mentorDao.getAllMentors();

        reminderDao = database.reminderDao();
        allReminders = reminderDao.getAllReminders();
    }

    //Terms=========================================================================================

    public void insert(Term term){
        new InsertTermAsyncTask(termDao).execute(term);
    }

    public void update(Term term){
        new UpdateTermAsyncTask(termDao).execute(term);
    }

    public void delete(Term term){
        new DeleteTermAsyncTask(termDao).execute(term);
    }

    public void deleteAllTerms(){
        new DeleteAllTermsAsyncTask(termDao).execute();
    }

    public LiveData<List<Term>> getAllTerms() {
        return allTerms;
    }

    private static class InsertTermAsyncTask extends AsyncTask<Term, Void, Void>{
        private TermDao termDao;

        private InsertTermAsyncTask(TermDao termDao){
            this.termDao = termDao;
        }

        @Override
        protected Void doInBackground(Term... terms) {
            long id = termDao.insert(terms[0]);
            terms[0].setTermId((int)id);
            return null;
        }
    }

    private static class UpdateTermAsyncTask extends AsyncTask<Term, Void, Void>{
        private TermDao termDao;

        private UpdateTermAsyncTask(TermDao termDao){
            this.termDao = termDao;
        }

        @Override
        protected Void doInBackground(Term... terms) {
            termDao.update(terms[0]);
            return null;
        }
    }

    private static class DeleteTermAsyncTask extends AsyncTask<Term, Void, Void>{
        private TermDao termDao;

        private DeleteTermAsyncTask(TermDao termDao){
            this.termDao = termDao;
        }

        @Override
        protected Void doInBackground(Term... terms) {
            termDao.delete(terms[0]);
            return null;
        }
    }

    private static class DeleteAllTermsAsyncTask extends AsyncTask<Void, Void, Void>{
        private TermDao termDao;

        private DeleteAllTermsAsyncTask(TermDao termDao){
            this.termDao = termDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            termDao.deleteAllTerms();
            return null;
        }
    }

    //Courses===================================================================================

    public void insert(Course course){
        new InsertCourseAsyncTask(courseDao).execute(course);
    }

    public void update(Course course){
        new UpdateCourseAsyncTask(courseDao).execute(course);
    }

    public void delete(Course course){
        new DeleteCourseAsyncTask(courseDao).execute(course);
    }

    public void deleteAllCourses(){
        new DeleteAllCoursesAsyncTask(courseDao).execute();
    }

    public LiveData<List<Course>> getAllCourses() {
        return allCourses;
    }

    private static class InsertCourseAsyncTask extends AsyncTask<Course, Void, Void>{
        private CourseDao courseDao;

        private InsertCourseAsyncTask(CourseDao courseDao){
            this.courseDao = courseDao;
        }

        @Override
        protected Void doInBackground(Course... courses) {
            long id = courseDao.insert(courses[0]);
            courses[0].setCourseId((int)id);
            return null;
        }
    }

    private static class UpdateCourseAsyncTask extends AsyncTask<Course, Void, Void>{
        private CourseDao courseDao;

        private UpdateCourseAsyncTask(CourseDao courseDao){
            this.courseDao = courseDao;
        }

        @Override
        protected Void doInBackground(Course... courses) {
            courseDao.update(courses[0]);
            return null;
        }
    }

    private static class DeleteCourseAsyncTask extends AsyncTask<Course, Void, Void>{
        private CourseDao courseDao;

        private DeleteCourseAsyncTask(CourseDao courseDao){
            this.courseDao = courseDao;
        }

        @Override
        protected Void doInBackground(Course... courses) {
            courseDao.delete(courses[0]);
            return null;
        }
    }

    private static class DeleteAllCoursesAsyncTask extends AsyncTask<Void, Void, Void>{
        private CourseDao courseDao;

        private DeleteAllCoursesAsyncTask(CourseDao courseDao){
            this.courseDao = courseDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            courseDao.deleteAllCourses();
            return null;
        }
    }

    //Assessments===================================================================================

    public void insert(Assessment assessment){
        new InsertAssessmentAsyncTask(assessmentDao).execute(assessment);
    }

    public void update(Assessment assessment){
        new UpdateAssessmentAsyncTask(assessmentDao).execute(assessment);
    }

    public void delete(Assessment assessment){
        new DeleteAssessmentAsyncTask(assessmentDao).execute(assessment);
    }

    public void deleteAllAssessments(){
        new DeleteAllAssessmentsAsyncTask(assessmentDao).execute();
    }

    public LiveData<List<Assessment>> getAllAssessments() {
        return allAssessments;
    }

    private static class InsertAssessmentAsyncTask extends AsyncTask<Assessment, Void, Void>{
        private AssessmentDao assessmentDao;

        private InsertAssessmentAsyncTask(AssessmentDao assessmentDao){
            this.assessmentDao = assessmentDao;
        }

        @Override
        protected Void doInBackground(Assessment... assessments) {
            long id = assessmentDao.insert(assessments[0]);
            assessments[0].setAssessmentId((int)id);
            return null;
        }
    }

    private static class UpdateAssessmentAsyncTask extends AsyncTask<Assessment, Void, Void>{
        private AssessmentDao assessmentDao;

        private UpdateAssessmentAsyncTask(AssessmentDao assessmentDao){
            this.assessmentDao = assessmentDao;
        }

        @Override
        protected Void doInBackground(Assessment... assessments) {
            assessmentDao.update(assessments[0]);
            return null;
        }
    }

    private static class DeleteAssessmentAsyncTask extends AsyncTask<Assessment, Void, Void>{
        private AssessmentDao assessmentDao;

        private DeleteAssessmentAsyncTask(AssessmentDao assessmentDao){
            this.assessmentDao = assessmentDao;
        }

        @Override
        protected Void doInBackground(Assessment... assessments) {
            assessmentDao.delete(assessments[0]);
            return null;
        }
    }

    private static class DeleteAllAssessmentsAsyncTask extends AsyncTask<Void, Void, Void>{
        private AssessmentDao assessmentDao;

        private DeleteAllAssessmentsAsyncTask(AssessmentDao assessmentDao){
            this.assessmentDao = assessmentDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            assessmentDao.deleteAllAssessments();
            return null;
        }
    }

    //Mentors===================================================================================

    public void insert(Mentor mentor){
        new InsertMentorAsyncTask(mentorDao).execute(mentor);
    }

    public void update(Mentor mentor){
        new UpdateMentorAsyncTask(mentorDao).execute(mentor);
    }

    public void delete(Mentor mentor){
        new DeleteMentorAsyncTask(mentorDao).execute(mentor);
    }

    public void deleteAllMentors(){
        new DeleteAllMentorsAsyncTask(mentorDao).execute();
    }

    public LiveData<List<Mentor>> getAllMentors() {
        return allMentors;
    }

    private static class InsertMentorAsyncTask extends AsyncTask<Mentor, Void, Void>{
        private MentorDao mentorDao;

        private InsertMentorAsyncTask(MentorDao mentorDao){
            this.mentorDao = mentorDao;
        }

        @Override
        protected Void doInBackground(Mentor... mentors) {
            long id = mentorDao.insert(mentors[0]);
            mentors[0].setMentorId((int)id);
            return null;
        }
    }

    private static class UpdateMentorAsyncTask extends AsyncTask<Mentor, Void, Void>{
        private MentorDao mentorDao;

        private UpdateMentorAsyncTask(MentorDao mentorDao){
            this.mentorDao = mentorDao;
        }

        @Override
        protected Void doInBackground(Mentor... mentors) {
            mentorDao.update(mentors[0]);
            return null;
        }
    }

    private static class DeleteMentorAsyncTask extends AsyncTask<Mentor, Void, Void>{
        private MentorDao mentorDao;

        private DeleteMentorAsyncTask(MentorDao mentorDao){
            this.mentorDao = mentorDao;
        }

        @Override
        protected Void doInBackground(Mentor... mentors) {
            mentorDao.delete(mentors[0]);
            return null;
        }
    }

    private static class DeleteAllMentorsAsyncTask extends AsyncTask<Void, Void, Void>{
        private MentorDao mentorDao;

        private DeleteAllMentorsAsyncTask(MentorDao mentorDao){
            this.mentorDao = mentorDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mentorDao.deleteAllMentors();
            return null;
        }
    }

    //Reminders===================================================================================

    public void insert(Reminder reminder){
        new InsertReminderAsyncTask(reminderDao).execute(reminder);
    }

    public void update(Reminder reminder){
        new UpdateReminderAsyncTask(reminderDao).execute(reminder);
    }

    public void delete(Reminder reminder){
        new DeleteReminderAsyncTask(reminderDao).execute(reminder);
    }

    public void deleteAllReminders(){
        new DeleteAllRemindersAsyncTask(reminderDao).execute();
    }

    public LiveData<List<Reminder>> getAllReminders() {
        return allReminders;
    }

    private static class InsertReminderAsyncTask extends AsyncTask<Reminder, Void, Void>{
        private ReminderDao reminderDao;

        private InsertReminderAsyncTask(ReminderDao reminderDao){
            this.reminderDao = reminderDao;
        }

        @Override
        protected Void doInBackground(Reminder... reminders) {
            long id = reminderDao.insert(reminders[0]);
            reminders[0].setReminderId((int)id);
            return null;
        }
    }

    private static class UpdateReminderAsyncTask extends AsyncTask<Reminder, Void, Void>{
        private ReminderDao reminderDao;

        private UpdateReminderAsyncTask(ReminderDao reminderDao){
            this.reminderDao = reminderDao;
        }

        @Override
        protected Void doInBackground(Reminder... reminders) {
            reminderDao.update(reminders[0]);
            return null;
        }
    }

    private static class DeleteReminderAsyncTask extends AsyncTask<Reminder, Void, Void>{
        private ReminderDao reminderDao;

        private DeleteReminderAsyncTask(ReminderDao reminderDao){
            this.reminderDao = reminderDao;
        }

        @Override
        protected Void doInBackground(Reminder... reminders) {
            reminderDao.delete(reminders[0]);
            return null;
        }
    }

    private static class DeleteAllRemindersAsyncTask extends AsyncTask<Void, Void, Void>{
        private ReminderDao reminderDao;

        private DeleteAllRemindersAsyncTask(ReminderDao reminderDao){
            this.reminderDao = reminderDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            reminderDao.deleteAllReminders();
            return null;
        }
    }
}
