package com.example.coursetracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBarDegree;
    private TextView textDegreePercent;
    private ProgressBar progressBarTerm;
    private TextView textTermPercent;
    private TextView textDegreeInfo;

    private CourseViewModel courseViewModel;
    private TermViewModel termViewModel;

    private LinkedHashMap<String, String> degreeInfo = new LinkedHashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBarDegree = findViewById(R.id.progressBarDegree);
        textDegreePercent = findViewById(R.id.textDegreePercent);
        progressBarTerm = findViewById(R.id.progressBarTerm);
        textTermPercent = findViewById(R.id.textTermPercent);
        textDegreeInfo = findViewById(R.id.textDegreeInfo);

        termViewModel = ViewModelProviders.of(this).get(TermViewModel.class);
        LiveData<List<Term>> allTerms = termViewModel.getAllTerms();
        allTerms.observe(this, terms -> {
            int termsPast = 0;
            String termCurrentTitle = "None";
            for (Term term : terms) {
                //Count past terms
                Date now = new Date();
                if(term.getEnd().before(now)){
                    termsPast++;
                }
                if(term.getStart().before(now) && term.getEnd().after(now)){
                    termCurrentTitle = term.getTitle();
                }
            }
            degreeInfo.put("Current Term", termCurrentTitle);
            degreeInfo.put("Terms Completed", termsPast + "/" + terms.size());
            updateDegreeInfo();

        });


        courseViewModel = ViewModelProviders.of(this).get(CourseViewModel.class);
        courseViewModel.getAllCourses().observe(this, new Observer<List<Course>>() {
            @Override
            public void onChanged(List<Course> courses) {
                double coursesPassed = 0;
                double coursesCurrent = 0;
                double coursesCurrentPassed = 0;
                for(Course course : courses){
                    //If the course is marked PASSED
                    if(course.getStatus().equals(Course.PASSED)){
                        coursesPassed++;
                    }
                    //If the course scheduled for now or is marked IN_PROGRESS
                    Date now = new Date();
                    if(course.getStatus().equals(Course.IN_PROGRESS) || course.getStartDate().before(now) && course.getEndDate().after(now)){
                        coursesCurrent++;
                        if(course.getStatus().equals(Course.PASSED)){
                            coursesCurrentPassed++;
                        }
                    }
                }
                double coursesCount = courses.size();
                int totalCoursesPassedPercent = (int)Math.round((coursesPassed / coursesCount) * 100);
                int currentCoursesPassedPercent = (int)Math.round((coursesCurrentPassed / coursesCurrent) * 100);
                progressBarDegree.setMax(courses.size());
                progressBarDegree.setProgress((int)coursesPassed, false);
                textDegreePercent.setText(String.valueOf(totalCoursesPassedPercent) + "%");
                progressBarTerm.setMax((int)coursesCurrent);
                progressBarTerm.setProgress((int)coursesCurrentPassed, false);
                textTermPercent.setText(String.valueOf(currentCoursesPassedPercent) + "%");

                degreeInfo.put("Term Progress", (int)coursesCurrentPassed + "/" + (int)coursesCurrent);
                degreeInfo.put("Overall Progress", (int)coursesPassed + "/" + (int)coursesCount);
                updateDegreeInfo();
            }
        });
    }

    public synchronized void updateDegreeInfo(){
        String info = "";
        String lf = "";
        for(Map.Entry<String, String> entry : degreeInfo.entrySet()){
            String key = entry.getKey();
            String value = entry.getValue();
            info += lf + key + ": " + value;
            lf = "\n";
        }
        textDegreeInfo.setText(info);
    }

    public void termsButtonOnClick(View view){
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }

    public void coursesButtonOnClick(View view){
        Intent intent = new Intent(this, CoursesActivity.class);
        startActivity(intent);
    }

    public void assessmentsButtonOnClick(View view){
        Intent intent = new Intent(this, AssessmentsActivity.class);
        startActivity(intent);
    }

    public void mentorsButtonOnClick(View view){
        Intent intent = new Intent(this, MentorsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.view_reminders:
                Intent intent = new Intent(this, RemindersActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_activity_menu, menu);
        return true;
    }
}
