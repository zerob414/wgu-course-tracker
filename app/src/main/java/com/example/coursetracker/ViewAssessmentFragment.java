package com.example.coursetracker;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AssessmentActionListener} interface
 * to handle interaction events.
 * Use the {@link ViewAssessmentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewAssessmentFragment extends Fragment {
    public static final String TAG = "com.example.coursetracker.ViewAssessmentFragment";

    private TextView titleText;
    private TextView typeText;
    private TextView startDateText;
    private TextView dueDateText;
    private TextView notesText;
    private TextView scoreText;
    private TextView courseText;

    private Assessment selectedAssessment;

    private AssessmentActionListener mListener;

    public ViewAssessmentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id    The assessment's SQLite/Room ID number, null if new Assessment
     * @param title  The assessment's title
     * @param type The assessment's type
     * @param startDate The assessment's start date
     * @param dueDate The assessment's due date
     * @param notes Notes for the assessment
     * @param score Score, out of 100, -1 means not scored
     * @param courseId Foreign key to this assessment's course
     * @return A new instance of fragment ViewAssessmentFragment.
     */
    public static ViewAssessmentFragment newInstance(int id, String title, String type,Date startDate, Date dueDate, String notes, int score, int courseId) {
        ViewAssessmentFragment fragment = new ViewAssessmentFragment();
        Bundle args = new Bundle();
        args.putInt(ViewAddEditAssessmentActivity.EXTRA_ID, id);
        args.putString(ViewAddEditAssessmentActivity.EXTRA_TITLE, title);
        args.putString(ViewAddEditAssessmentActivity.EXTRA_TYPE, type);
        args.putLong(ViewAddEditAssessmentActivity.EXTRA_START_DATE, startDate.getTime());
        args.putLong(ViewAddEditAssessmentActivity.EXTRA_DUE_DATE, dueDate.getTime());
        args.putString(ViewAddEditAssessmentActivity.EXTRA_NOTES, notes);
        args.putInt(ViewAddEditAssessmentActivity.EXTRA_SCORE, score);
        args.putInt(ViewAddEditAssessmentActivity.EXTRA_COURSE_ID, courseId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        titleText = getView().findViewById(R.id.textTitle);
        typeText = getView().findViewById(R.id.textType);
        startDateText = getView().findViewById(R.id.textStartDate);
        dueDateText = getView().findViewById(R.id.textDueDate);
        notesText = getView().findViewById(R.id.textNotes);
        scoreText = getView().findViewById(R.id.textScore);
        courseText = getView().findViewById(R.id.textCourse);

        getActivity().setTitle("View Assessment");

        if(selectedAssessment != null) {
            titleText.setText(selectedAssessment.getTitle());
            typeText.setText(selectedAssessment.getType());
            startDateText.setText(CommonDateFormatter.getInstance().format(selectedAssessment.getStartDate()));
            dueDateText.setText(CommonDateFormatter.getInstance().format(selectedAssessment.getDueDate()));
            notesText.setText(selectedAssessment.getNotes());
            if (selectedAssessment.getScore() >= 0) {
                scoreText.setText(selectedAssessment.getScore() + "%");
            } else {
                scoreText.setText("Not Scored");
            }
            CourseViewModel courseViewModel = ViewModelProviders.of(this).get(CourseViewModel.class);
            LiveData<List<Course>> courseList = courseViewModel.getAllCourses();
            courseList.observe(this, courses -> {
                for (Course course : courses) {
                    if (course.getCourseId() == selectedAssessment.getCourseId()) {
                        courseText.setText(course.getTitle());
                        break;
                    }
                }
            });
        }
    }

    public  void setAssessment(Assessment assessment){
        selectedAssessment = assessment;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menuInflater.inflate(R.menu.view_assessment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.add_assessment_reminder:
                mListener.onAddReminderClick();
                return true;
            case R.id.share:
                mListener.share();
                return true;
            case R.id.edit_assessment:
                mListener.onEditClick();
                return true;
            case android.R.id.home:
                mListener.onHomeClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_assessment, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AssessmentActionListener) {
            mListener = (AssessmentActionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement AssessmentActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Interface to send events back to the Activity.
     */
    public interface AssessmentActionListener {
        void onAddReminderClick();
        void onEditClick();
        void onHomeClick();
        void share();
    }
}
