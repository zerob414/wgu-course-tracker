package com.example.coursetracker;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnSaveMentorClickInterface} interface
 * to handle interaction events.
 * Use the {@link EditMentorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditMentorFragment extends Fragment implements View.OnFocusChangeListener {
    public static final String TAG = "com.example.coursetracker.EditMentorFragment";

    private EditText nameText;
    private EditText phoneText;
    private EditText emailText;

    private Mentor selectedMentor;

    private OnSaveMentorClickInterface mListener;

    public EditMentorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id    The mentor's SQLite/Room ID number, null if new Mentor
     * @param name  The mentor's name
     * @param phone The mentor's phone number
     * @param email The mentor's email address
     * @return A new instance of fragment EditMentorFragment.
     */
    public static EditMentorFragment newInstance(Integer id, String name, String phone, String email) {
        EditMentorFragment fragment = new EditMentorFragment();
        Bundle args = new Bundle();
        args.putInt(ViewAddEditMentorActivity.EXTRA_ID, id);
        args.putString(ViewAddEditMentorActivity.EXTRA_NAME, name);
        args.putString(ViewAddEditMentorActivity.EXTRA_PHONE, phone);
        args.putString(ViewAddEditMentorActivity.EXTRA_EMAIL, email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        nameText = getView().findViewById(R.id.nameText);
        phoneText = getView().findViewById(R.id.phoneText);
        emailText = getView().findViewById(R.id.emailText);

        nameText.setOnFocusChangeListener(this);
        nameText.setOnFocusChangeListener(this);
        nameText.setOnFocusChangeListener(this);

        getActivity().setTitle("Add Mentor");
        if(selectedMentor != null) {
            if(selectedMentor.getMentorId() > 0){
                getActivity().setTitle("Edit Mentor");
            }
            nameText.setText(selectedMentor.getName());
            phoneText.setText(selectedMentor.getPhone());
            emailText.setText(selectedMentor.getEmail());
        }
    }

    public void setMentor(Mentor mentor){
        selectedMentor = mentor;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menuInflater.inflate(R.menu.edit_mentor_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        //Toast.makeText(this.getContext(), "OptiosnItem Up pressed", Toast.LENGTH_SHORT).show();
        switch (item.getItemId()){
            case R.id.save_mentor:
                saveMentor();
                return true;
            case android.R.id.home:
                mListener.onUpClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveMentor(){
        if(nameText.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please enter a name", Toast.LENGTH_SHORT).show();
        }
        else if(phoneText.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please enter a phone number", Toast.LENGTH_SHORT).show();
        }
        else if(emailText.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please enter an email address", Toast.LENGTH_SHORT).show();
        }
        else {
            if(selectedMentor == null)selectedMentor = new Mentor();
            selectedMentor.setName(nameText.getText().toString());
            selectedMentor.setPhone(phoneText.getText().toString());
            selectedMentor.setEmail(emailText.getText().toString());
            mListener.onSaveClick(selectedMentor);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_mentor, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSaveMentorClickInterface) {
            mListener = (OnSaveMentorClickInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MentorActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(view instanceof EditText && !hasFocus){
            InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Interface to send events back to the Activity.

     */
    public interface OnSaveMentorClickInterface {
        void onSaveClick(Mentor mentor);
        void onUpClick();
    }
}
