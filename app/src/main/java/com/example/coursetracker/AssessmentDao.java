package com.example.coursetracker;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface AssessmentDao {
    @Insert
    long insert(Assessment assessment);

    @Update
    void update(Assessment assessment);

    @Delete
    void delete(Assessment assessment);

    @Query("DELETE FROM tbl_assessments")
    void deleteAllAssessments();

    @Query("SELECT * FROM tbl_assessments ORDER BY title ASC")
    LiveData<List<Assessment>> getAllAssessments();
}
