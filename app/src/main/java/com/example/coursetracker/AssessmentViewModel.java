package com.example.coursetracker;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class AssessmentViewModel extends AndroidViewModel {
    private CourseTrackerRepository repository;
    private LiveData<List<Assessment>> allAssessments;

    public AssessmentViewModel(@NonNull Application application) {
        super(application);
        repository = new CourseTrackerRepository(application);
        allAssessments = repository.getAllAssessments();
    }

    public void insert(Assessment assessment){
        repository.insert(assessment);
    }

    public void update(Assessment assessment){
        repository.update(assessment);
    }

    public void delete(Assessment assessment){
        repository.delete(assessment);
    }

    public void deleteAllAssessments(){
        repository.deleteAllAssessments();
    }

    public LiveData<List<Assessment>> getAllAssessments(){
        return allAssessments;
    }
}
