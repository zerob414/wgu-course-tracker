package com.example.coursetracker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnEditReminderClickInterface} interface
 * to handle interaction events.
 * Use the {@link ViewReminderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewReminderFragment extends Fragment {
    public static final String TAG = "com.example.coursetracker.ViewReminderFragment";

    private TextView textTitle;
    private TextView textDate;
    private TextView textEntityType;
    private TextView textEntityTitle;
    private TextView textMessage;

    private Reminder selectedReminder;

    private OnEditReminderClickInterface mListener;

    public ViewReminderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id          The reminder's SQLite/Room ID number
     * @param title       The reminder's title
     * @param date        The reminder's date
     * @param entityType  The reminder's type
     * @param entityId    The ID of the entity this reminder is for
     * @param entityTitle The title of the entity this reminder is for
     * @return A new instance of fragment ViewReminderFragment.
     */
    public static ViewReminderFragment newInstance(int id, String title, Date date, String entityType, int entityId, String entityTitle, String message) {
        ViewReminderFragment fragment = new ViewReminderFragment();
        Bundle args = new Bundle();
        args.putInt(ViewAddEditReminderActivity.EXTRA_ID, id);
        args.putString(ViewAddEditReminderActivity.EXTRA_TITLE, title);
        args.putString(ViewAddEditReminderActivity.EXTRA_ENTITY_TYPE, entityType);
        args.putString(ViewAddEditReminderActivity.EXTRA_ENTITY_TITLE, entityTitle);
        args.putLong(ViewAddEditReminderActivity.EXTRA_DATE, date.getTime());
        args.putInt(ViewAddEditReminderActivity.EXTRA_ENTITY_ID, entityId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        textTitle = getView().findViewById(R.id.textTitle);
        textEntityType = getView().findViewById(R.id.textEntityType);
        textEntityTitle = getView().findViewById(R.id.textEntityName);
        textDate = getView().findViewById(R.id.textDate);
        textMessage = getView().findViewById(R.id.textMessage);

        getActivity().setTitle("View Reminder");

        if(selectedReminder != null){
            textTitle.setText(selectedReminder.getTitle());
            textEntityType.setText(selectedReminder.getEntityType() + ":");
            textEntityTitle.setText(selectedReminder.getEntityTitle());
            textDate.setText(CommonDateFormatter.getInstance().formatWithTime(selectedReminder.getDate(), getContext()));
            textMessage.setText(selectedReminder.getMessage());
        }
    }

    public void setReminder(Reminder reminder){
        selectedReminder = reminder;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menuInflater.inflate(R.menu.view_reminder_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.edit_reminder:
                mListener.onEditClick();
                return true;
            case android.R.id.home:
                mListener.onHomeClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_reminder, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEditReminderClickInterface) {
            mListener = (OnEditReminderClickInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEditReminderClickInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Interface to send events back to the Activity.
     */
    public interface OnEditReminderClickInterface {
        void onEditClick();
        void onHomeClick();
    }
}
