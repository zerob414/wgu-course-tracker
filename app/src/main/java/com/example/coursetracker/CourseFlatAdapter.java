package com.example.coursetracker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CourseFlatAdapter extends RecyclerView.Adapter<CourseFlatAdapter.CourseHolder> {
    private List<Course> courses = new ArrayList<>();
    private OnCourseClickListener listener;

    @NonNull
    @Override
    public CourseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.course_cardview_flat, parent, false);
        return new CourseHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseHolder holder, int position) {
        Course currentCourse = courses.get(position);
        holder.textTitle.setText(currentCourse.getTitle());
        holder.textStatus.setText(currentCourse.getStatus());
        String date = "Start Date: " + CommonDateFormatter.getInstance().format(currentCourse.getStartDate());
        holder.textStartDate.setText(date);
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public Course getCourseAt(int index){
        return courses.get(index);
    }

    public void setCourses(List<Course> courses){
        this.courses = courses;
        notifyDataSetChanged();
    }

    class CourseHolder extends RecyclerView.ViewHolder{
        private TextView textTitle;
        private TextView textStatus;
        private TextView textStartDate;

        public CourseHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.course_title);
            textStatus = itemView.findViewById(R.id.course_status);
            textStartDate = itemView.findViewById(R.id.course_start);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int index = getAdapterPosition();
                    if(listener != null && index != RecyclerView.NO_POSITION) {
                        listener.onCourseClick(courses.get(index));
                    }
                }
            });
        }
    }

    public interface OnCourseClickListener {
        void onCourseClick(Course course);
    }

    public void setOnCourseClickListener(CourseFlatAdapter.OnCourseClickListener listener){
        this.listener = listener;
    }
}
