package com.example.coursetracker;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Observer;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnSaveAssessmentClickInterface} interface
 * to handle interaction events.
 * Use the {@link EditAssessmentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditAssessmentFragment extends Fragment implements View.OnClickListener, TextWatcher, View.OnFocusChangeListener {
    public static final String TAG = "com.example.coursetracker.EditAssessmentFragment";

    private EditText titleText;
    private RadioButton objectiveRadio;
    private RadioButton performanceRadio;
    private Button startDateButton;
    private Button dueDateButton;
    private EditText notesText;
    private EditText scoreText;
    private CheckBox noScoreCheck;
    private Spinner courseSpinner;

    private Assessment selectedAssessment;

    private CourseViewModel courseViewModel;

    private OnSaveAssessmentClickInterface mListener;

    public EditAssessmentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id    The assessment's SQLite/Room ID number, null if new Assessment
     * @param title  The assessment's title
     * @param type The assessment's type
     * @param dueDate The assessment's due date
     * @param notes Notes for the assessment
     * @param score Score, out of 100, -1 means not scored
     * @param courseId Foreign key to this assessment's course
     * @return A new instance of fragment EditAssessmentFragment.
     */
    public static EditAssessmentFragment newInstance(int id, String title, String type, Date startDate, Date dueDate, String notes, int score, int courseId) {
        EditAssessmentFragment fragment = new EditAssessmentFragment();
        Bundle args = new Bundle();
        args.putInt(ViewAddEditAssessmentActivity.EXTRA_ID, id);
        args.putString(ViewAddEditAssessmentActivity.EXTRA_TITLE, title);
        args.putString(ViewAddEditAssessmentActivity.EXTRA_TYPE, type);
        args.putLong(ViewAddEditAssessmentActivity.EXTRA_START_DATE, startDate.getTime());
        args.putLong(ViewAddEditAssessmentActivity.EXTRA_DUE_DATE, dueDate.getTime());
        args.putString(ViewAddEditAssessmentActivity.EXTRA_NOTES, notes);
        args.putInt(ViewAddEditAssessmentActivity.EXTRA_SCORE, score);
        args.putInt(ViewAddEditAssessmentActivity.EXTRA_COURSE_ID, courseId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void setAssessment(Assessment assessment){
        selectedAssessment = assessment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        titleText = getView().findViewById(R.id.textTitle);
        objectiveRadio = getView().findViewById(R.id.radioObjective);
        performanceRadio = getView().findViewById(R.id.radioPerformance);
        startDateButton = getView().findViewById(R.id.buttonStartDate);
        dueDateButton = getView().findViewById(R.id.buttonDueDate);
        notesText = getView().findViewById(R.id.textNotes);
        scoreText = getView().findViewById(R.id.textScore);
        noScoreCheck = getView().findViewById(R.id.checkNoScore);
        courseSpinner = getView().findViewById(R.id.spinnerCourse);

        titleText.setOnFocusChangeListener(this);
        notesText.setOnFocusChangeListener(this);
        scoreText.setOnFocusChangeListener(this);

        getActivity().setTitle("Add Assessment");
        if(selectedAssessment != null) {
            if(selectedAssessment.getAssessmentId() > 0){
                getActivity().setTitle("Edit Assessment");
            }
            titleText.setText(selectedAssessment.getTitle());
            boolean assessmentIsObjective = selectedAssessment.getType() == null || selectedAssessment.getType().equals(Assessment.OBJECTIVE);
            objectiveRadio.setSelected(assessmentIsObjective);
            performanceRadio.setSelected(!assessmentIsObjective);
            startDateButton.setText(CommonDateFormatter.getInstance().format(selectedAssessment.getStartDate()));
            dueDateButton.setText(CommonDateFormatter.getInstance().format(selectedAssessment.getDueDate()));
            notesText.setText(selectedAssessment.getNotes());
            if (selectedAssessment.getScore() >= 0) {
                scoreText.setText(String.valueOf(selectedAssessment.getScore()));
                noScoreCheck.setSelected(false);
            } else {
                scoreText.setText("");
                noScoreCheck.setSelected(true);
            }
            courseViewModel = ViewModelProviders.of(this).get(CourseViewModel.class);
            LiveData<List<Course>> courseList = courseViewModel.getAllCourses();
            ArrayAdapter<Course> courseArrayAdapter = new ArrayAdapter<Course>(this.getContext(), android.R.layout.simple_spinner_item);
            courseList.observe(this, courses -> {
                int selectedIndex = 0;
                for (Course course : courses) {
                    courseArrayAdapter.add(course);
                    if (course.getCourseId() == selectedAssessment.getCourseId()) {
                        selectedIndex = courses.indexOf(course);
                    }
                }
                courseSpinner.setSelection(selectedIndex);
            });
            courseSpinner.setAdapter(courseArrayAdapter);
        }

        scoreText.addTextChangedListener(this);
        startDateButton.setOnClickListener(this);
        dueDateButton.setOnClickListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menuInflater.inflate(R.menu.edit_assessment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.save_assessment:
                saveAssessment();
                return true;
            case android.R.id.home:
                mListener.onUpClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveAssessment(){
        if(titleText.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please enter a title", Toast.LENGTH_SHORT).show();
        }
        else if(startDateButton.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please select a start date", Toast.LENGTH_SHORT).show();
        }
        else if(dueDateButton.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please select a due date", Toast.LENGTH_SHORT).show();
        }
        else if(scoreText.getText().toString().trim().isEmpty() && !noScoreCheck.isSelected()){
            Toast.makeText(getContext(), "Please enter a score or select 'No Score'", Toast.LENGTH_SHORT).show();
        }
        else {
            if(selectedAssessment == null)selectedAssessment = new Assessment();
            selectedAssessment.setTitle(titleText.getText().toString());
            selectedAssessment.setType(objectiveRadio.isChecked() ? Assessment.OBJECTIVE : Assessment.PERFORMANCE);
            Date assessmentStartDate = CommonDateFormatter.parse(startDateButton.getText().toString());
            if(assessmentStartDate != null) {
                selectedAssessment.setStartDate(assessmentStartDate);
            }
            Date assessmentDueDate = CommonDateFormatter.parse(dueDateButton.getText().toString());
            if(assessmentDueDate != null) {
                selectedAssessment.setDueDate(assessmentDueDate);
            }
            selectedAssessment.setNotes(notesText.getText().toString());
            selectedAssessment.setScore(noScoreCheck.isChecked() ? Assessment.NO_SCORE : Integer.valueOf(scoreText.getText().toString()));
            Course assessmentCourse = (Course) courseSpinner.getSelectedItem();
            selectedAssessment.setCourseId(assessmentCourse == null ? Assessment.NO_COURSE : assessmentCourse.getCourseId());
            mListener.onSaveClick(selectedAssessment);
        }
    }

    @Override
    public void onClick(View view) {
        if(view == startDateButton){
            Date assessmentStartDate = CommonDateFormatter.parse(startDateButton.getText().toString());
            if(assessmentStartDate == null){
                assessmentStartDate = new Date();
            }
            Calendar cal = new GregorianCalendar();
            cal.setTime(assessmentStartDate);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar cal = new GregorianCalendar();
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, monthOfYear);
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    startDateButton.setText(CommonDateFormatter.getInstance().format(cal.getTime()));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
        else if(view == dueDateButton){
            Date assessmentDueDate = CommonDateFormatter.parse(dueDateButton.getText().toString());
            if(assessmentDueDate == null){
                assessmentDueDate = new Date();
            }
            Calendar cal = new GregorianCalendar();
            cal.setTime(assessmentDueDate);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            Calendar cal = new GregorianCalendar();
                            cal.set(Calendar.YEAR, year);
                            cal.set(Calendar.MONTH, monthOfYear);
                            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            dueDateButton.setText(CommonDateFormatter.getInstance().format(cal.getTime()));
                        }
                    }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_assessment, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSaveAssessmentClickInterface) {
            mListener = (OnSaveAssessmentClickInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement AssessmentActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        noScoreCheck.setChecked(scoreText.getText().toString().trim().isEmpty());
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(view instanceof EditText && !hasFocus){
            InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Interface to send events back to the Activity.

     */
    public interface OnSaveAssessmentClickInterface {
        void onSaveClick(Assessment assessment);
        void onUpClick();
    }
}
