package com.example.coursetracker;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Entity(tableName = "tbl_terms")
public class Term {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "term_id")
    private int termId;
    private String title;
    private Date start;
    private Date end;
    private int number;
    private String notes;
    @Embedded
    private CourseCountHolder courseCountHolder;

    public Term(String title, Date start, Date end, int number, String notes) {
        this.title = title;
        this.start = start;
        this.end = end;
        this.number = number;
        this.notes = notes;
        this.courseCountHolder = courseCountHolder;
    }

    @Ignore
    public Term(){
        start = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(start);
        cal.add(Calendar.MONTH, 6);
        end = cal.getTime();
    }

    public int getTermId() {
        return termId;
    }

    public void setTermId(int termId) {
        this.termId = termId;
    }

    public String getTitle() {
        return title;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    public int getNumber() {
        return number;
    }

    public CourseCountHolder getCourseCountHolder() {
        return courseCountHolder;
    }

    public int getCourseCount(){
        if(courseCountHolder == null)return -1;
        return courseCountHolder.course_count;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setCourseCountHolder(CourseCountHolder courseCountHolder) {
        this.courseCountHolder = courseCountHolder;
    }

    @NonNull
    @Override
    public String toString() {
        return number + " - " + title;
    }
}

class CourseCountHolder{
    int course_count;
}