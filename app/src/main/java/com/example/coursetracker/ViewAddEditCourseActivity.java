package com.example.coursetracker;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class ViewAddEditCourseActivity extends AppCompatActivity implements ViewCourseFragment.CourseActionListener, EditCourseFragment.OnSaveCourseClickInterface {
    public static final String EXTRA_ID = "com.example.coursetracker.EXTRA_ID";
    public static final String EXTRA_TITLE = "com.example.coursetracker.EXTRA_TITLE";
    public static final String EXTRA_START_DATE = "com.example.coursetracker.EXTRA_START_DATE";
    public static final String EXTRA_END_DATE = "com.example.coursetracker.EXTRA_END_DATE";
    public static final String EXTRA_STATUS = "com.example.coursetracker.EXTRA_STATUS";
    public static final String EXTRA_NOTES = "com.example.coursetracker.EXTRA_NOTES";
    public static final String EXTRA_MENTOR_ID = "com.example.coursetracker.EXTRA_MENTOR_ID";
    public static final String EXTRA_TERM_ID = "com.example.coursetracker.EXTRA_TERM_ID";

    private CourseViewModel courseViewModel;
    private Bundle extras;
    private Course selectedCourse;

    private ViewCourseFragment viewCourseFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_course);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        courseViewModel = ViewModelProviders.of(this).get(CourseViewModel.class);
        Intent intent = getIntent();
        if(intent == null){
            Toast.makeText(this, "An error occurred, please restart the app", Toast.LENGTH_SHORT).show();
        }
        else{
            extras = intent.getExtras();
            if(extras == null){
                extras = new Bundle();
            }
            if(extras.getInt(EXTRA_ID) == 0){
                switchToEditCourse(new Course());
            }
            else {
                LiveData<List<Course>> allCourses = courseViewModel.getAllCourses();
                allCourses.observe(this, courses -> {
                    for(Course course : courses){
                        if(course.getCourseId() == extras.getInt(EXTRA_ID)) {
                            selectedCourse = course;
                            switchToViewCourse(selectedCourse);
                            break;
                        }
                    }
                });
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Fragment fragment = getFragmentManager().findFragmentByTag(EditCourseFragment.TAG);
        if (fragment != null && fragment.isVisible() && selectedCourse != null) {
            switchToViewCourse(selectedCourse);
        }
        else {
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onAddReminderClick(){
        if(selectedCourse != null) {
            CharSequence[] options = new CharSequence[]{getApplicationContext().getString(R.string.start_date), getApplicationContext().getString(R.string.end_date)};

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Create a reminder for:");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Date dueDate = selectedCourse.getStartDate();
                    String verb = "starting";
                    if(which == 1){
                        dueDate = selectedCourse.getEndDate();
                        verb = "ending";
                    }
                    String date = CommonDateFormatter.getInstance().format(dueDate);
                    Calendar calDueDate = new GregorianCalendar();
                    calDueDate.setTime(dueDate);
                    Calendar calDayBefore = new GregorianCalendar();
                    calDayBefore.set(Calendar.YEAR, calDueDate.get(Calendar.YEAR));
                    calDayBefore.set(Calendar.MONTH, calDueDate.get(Calendar.MONTH));
                    calDayBefore.set(Calendar.DAY_OF_MONTH, calDueDate.get(Calendar.DAY_OF_MONTH));
                    calDayBefore.add(Calendar.DAY_OF_MONTH, -1);
                    Intent intent = new Intent(ViewAddEditCourseActivity.this, ViewAddEditReminderActivity.class);
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ID, 0);
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_TITLE, selectedCourse.getTitle() + " will be " + verb + " soon!");
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_MESSAGE, "The course " + selectedCourse.getTitle() + " will be " + verb + " on " + date + ".");
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ENTITY_ID, selectedCourse.getCourseId());
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ENTITY_TITLE, selectedCourse.getTitle());
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ENTITY_TYPE, Reminder.TYPE_COURSE);
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_DATE, calDayBefore.getTime().getTime());
                    startActivity(intent);
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //the user clicked on Cancel
                }
            });
            builder.show();
        }
    }

    @Override
    public void share(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, selectedCourse.getNotes());
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Check out my notes for " + selectedCourse.getTitle());
        sendIntent.setType("text/plain");
        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }

    @Override
    public void onEditClick() {
        if(extras == null){
            switchToEditCourse(new Course());
        }
        else{
            switchToEditCourse(selectedCourse);
        }
    }

    @Override
    public void onSaveClick(Course course) {
        if(course != null){
            selectedCourse = course;
            if(course.getCourseId() == 0){
                courseViewModel.insert(course);
            }
            else {
                courseViewModel.update(course);
            }
        }
        switchToViewCourse(selectedCourse);
    }

    @Override
    public void onUpClick() {
        onBackPressed();
    }

    @Override
    public void onHomeClick(){
        onBackPressed();
    }

    @Override
    public void onBackPressed(){
        Fragment fragment = getFragmentManager().findFragmentByTag(EditCourseFragment.TAG);
        if (fragment != null && fragment.isVisible() && selectedCourse != null) {
            switchToViewCourse(selectedCourse);
        }
        else {
            finish();
        }
    }

    private void switchToEditCourse(Course course) {
        EditCourseFragment newFragment = new EditCourseFragment();
        newFragment.setCourse(course);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment, EditCourseFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void switchToViewCourse(Course course) {
        ViewCourseFragment newFragment = new ViewCourseFragment();
        newFragment.setCourse(course);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment, ViewCourseFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
        viewCourseFragment = newFragment;
    }

    public void viewMentor(View view){
        if(selectedCourse != null && selectedCourse.getMentorId() > 0) {
            Intent intent = new Intent(this, ViewAddEditMentorActivity.class);
            intent.putExtra(ViewAddEditMentorActivity.EXTRA_ID, selectedCourse.getMentorId());
            startActivity(intent);
        }
    }

    public void onButtonPhoneClick(View view){
        call();
    }

    public void onButtonEmailClick(View view){
        email();
    }

    @Override
    public void call() {
        if (viewCourseFragment != null) {
            String phoneNumber = viewCourseFragment.getSelectedCourseMentor().getPhone();
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
            startActivity(intent);
        }
    }

    @Override
    public void email() {
        if (viewCourseFragment != null) {
            String emailAddress = viewCourseFragment.getSelectedCourseMentor().getEmail();
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            String[] addresses = {emailAddress};
            intent.putExtra(Intent.EXTRA_EMAIL, addresses);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }
}
