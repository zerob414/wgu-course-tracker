package com.example.coursetracker;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class ViewAddEditTermActivity extends AppCompatActivity implements ViewTermFragment.TermActionListener, EditTermFragment.OnSaveTermClickInterface {
    public static final String EXTRA_ID = "com.example.coursetracker.EXTRA_ID";
    public static final String EXTRA_TITLE = "com.example.coursetracker.EXTRA_NAME";
    public static final String EXTRA_START_DATE = "com.example.coursetracker.EXTRA_START_DATE";
    public static final String EXTRA_END_DATE = "com.example.coursetracker.EXTRA_END_DATE";
    public static final String EXTRA_NUMBER = "com.example.coursetracker.EXTRA_NUMBER";
    public static final String EXTRA_NOTES = "com.example.coursetracker.EXTRA_NOTES";


    public static final String EXTRA_TERM_COUNT = "com.example.coursetracker.EXTRA_TERM_COUNT";

    private TermViewModel termViewModel;
    private Bundle extras;
    private Term selectedTerm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_term);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        termViewModel = ViewModelProviders.of(this).get(TermViewModel.class);
        Intent intent = getIntent();
        if(intent == null){
            Toast.makeText(this, "An error occurred, please restart the app", Toast.LENGTH_SHORT).show();
        }
        else{
            extras = intent.getExtras();
            if(extras == null){
                extras = new Bundle();
            }
            if(extras.getInt(EXTRA_ID) == 0){
                switchToEditTerm(new Term());
            }
            else {
                LiveData<List<Term>> allTerms = termViewModel.getAllTerms();
                allTerms.observe(this, terms -> {
                    for(Term term : terms){
                        if(term.getTermId() == extras.getInt(EXTRA_ID)) {
                            selectedTerm = term;
                            switchToViewTerm(selectedTerm);
                            break;
                        }
                    }
                });
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Fragment fragment = getFragmentManager().findFragmentByTag(EditTermFragment.TAG);
        if (fragment != null && fragment.isVisible() && selectedTerm != null) {
            switchToViewTerm(selectedTerm);
        }
        else {
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onAddReminderClick(){
        if(selectedTerm != null) {
            CharSequence[] options = new CharSequence[]{getApplicationContext().getString(R.string.start_date), getApplicationContext().getString(R.string.end_date)};

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Select your option:");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Date dueDate = selectedTerm.getStart();
                    String verb = "starting";
                    if(which == 1){
                        dueDate = selectedTerm.getEnd();
                        verb = "ending";
                    }
                    String date = CommonDateFormatter.getInstance().format(dueDate);
                    Calendar calDueDate = new GregorianCalendar();
                    calDueDate.setTime(dueDate);
                    Calendar calDayBefore = new GregorianCalendar();
                    calDayBefore.set(Calendar.YEAR, calDueDate.get(Calendar.YEAR));
                    calDayBefore.set(Calendar.MONTH, calDueDate.get(Calendar.MONTH));
                    calDayBefore.set(Calendar.DAY_OF_MONTH, calDueDate.get(Calendar.DAY_OF_MONTH));
                    calDayBefore.add(Calendar.DAY_OF_MONTH, -1);
                    Intent intent = new Intent(ViewAddEditTermActivity.this, ViewAddEditReminderActivity.class);
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ID, 0);
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_TITLE, selectedTerm.getTitle() + " will be " + verb + " soon!");
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_MESSAGE, "The term " + selectedTerm.getTitle() + " will be " + verb + " on " + date + ".");
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ENTITY_ID, selectedTerm.getTermId());
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ENTITY_TITLE, selectedTerm.getTitle());
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ENTITY_TYPE, Reminder.TYPE_TERM);
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_DATE, calDayBefore.getTime().getTime());
                    startActivity(intent);
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //the user clicked on Cancel
                }
            });
            builder.show();
        }
    }

    @Override
    public void share(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, selectedTerm.getNotes());
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Check out my notes for " + selectedTerm.getTitle());
        sendIntent.setType("text/plain");
        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }

    @Override
    public void onEditClick() {
        if(extras == null){
            switchToEditTerm(new Term());
        }
        else{
            switchToEditTerm(selectedTerm);
        }
    }

    @Override
    public void onSaveClick(Term term) {
        if(term != null){
            selectedTerm = term;
            if(term.getTermId() == 0){
                termViewModel.insert(term);
            }
            else {
                termViewModel.update(term);
            }
        }
        switchToViewTerm(selectedTerm);
    }

    @Override
    public void onUpClick() {
        onBackPressed();
    }

    @Override
    public void onHomeClick(){
        onBackPressed();
    }

    @Override
    public void onBackPressed(){
        Fragment fragment = getFragmentManager().findFragmentByTag(EditTermFragment.TAG);
        if (fragment != null && fragment.isVisible() && selectedTerm != null) {
            switchToViewTerm(selectedTerm);
        }
        else {
            finish();
        }
    }

    private void switchToEditTerm(Term term) {
        EditTermFragment newFragment = new EditTermFragment();
        newFragment.setTerm(term);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment, EditTermFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void switchToViewTerm(Term term) {
        ViewTermFragment newFragment = new ViewTermFragment();
        newFragment.setTerm(term);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment, ViewTermFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
