package com.example.coursetracker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TermAdapter extends RecyclerView.Adapter<TermAdapter.TermHolder> {
    private List<Term> terms = new ArrayList<>();
    private OnTermClickListener listener;

    @NonNull
    @Override
    public TermHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.term_cardview, parent, false);
        return new TermHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TermHolder holder, int position) {
        Term currentTerm = terms.get(position);
        holder.textTitle.setText(currentTerm.getTitle());
        holder.textNumber.setText(String.valueOf(currentTerm.getNumber()));
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM d YYYY");
        String dates = dateFormatter.format(currentTerm.getStart()) + " to " + dateFormatter.format(currentTerm.getEnd());
        holder.textDates.setText(dates);
    }

    @Override
    public int getItemCount() {
        return terms.size();
    }

    public Term getTermAt(int index){ return terms.get(index); }

    public void setTerms(List<Term> terms){
        this.terms = terms;
        notifyDataSetChanged();
    }

    class TermHolder extends RecyclerView.ViewHolder{
        private TextView textTitle;
        private TextView textNumber;
        private TextView textDates;

        public TermHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.term_title);
            textNumber = itemView.findViewById(R.id.term_number);
            textDates = itemView.findViewById(R.id.term_dates);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int index = getAdapterPosition();
                    if(listener != null && index != RecyclerView.NO_POSITION) {
                        listener.onTermClick(terms.get(index));
                    }
                }
            });
        }
    }

    public interface OnTermClickListener {
        void onTermClick(Term term);
    }

    public void setOnTermClickListener(OnTermClickListener listener){
        this.listener = listener;
    }
}
