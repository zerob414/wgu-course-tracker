package com.example.coursetracker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MentorAdapter extends RecyclerView.Adapter<MentorAdapter.MentorHolder> {
    private List<Mentor> mentors = new ArrayList<>();
    private OnMentorClickListener listener;

    @NonNull
    @Override
    public MentorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mentor_cardview, parent, false);
        return new MentorHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MentorHolder holder, int position) {
        Mentor currentMentor = mentors.get(position);
        holder.textName.setText(currentMentor.getName());
        //holder.textCourses.setText(currentMentor.getCoursesString());
    }

    @Override
    public int getItemCount() {
        return mentors.size();
    }

    public Mentor getMentorAt(int index){
        return mentors.get(index);
    }

    public void setMentors(List<Mentor> mentors){
        this.mentors = mentors;
        notifyDataSetChanged();
    }

    class MentorHolder extends RecyclerView.ViewHolder{
        private TextView textName;
        private TextView textCourses;

        public MentorHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.mentor_name);
            textCourses = itemView.findViewById(R.id.mentor_courses);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int index = getAdapterPosition();
                    if(listener != null && index != RecyclerView.NO_POSITION) {
                        listener.onMentorClick(mentors.get(index));
                    }
                }
            });
        }
    }

    public interface OnMentorClickListener {
        void onMentorClick(Mentor mentor);
    }

    public void setOnMentorClickListener(OnMentorClickListener listener){
        this.listener = listener;
    }
}
