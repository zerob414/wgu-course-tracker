package com.example.coursetracker;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface TermDao {
    @Insert
    long insert(Term term);

    @Update
    void update(Term term);

    @Delete
    void delete(Term term);

    @Query("DELETE FROM tbl_terms")
    void deleteAllTerms();

    @Query("SELECT term_id, title, start, `end`, number, (SELECT COUNT(course_id) FROM tbl_courses WHERE tbl_courses.term_id = tbl_terms.term_id) AS course_count FROM tbl_terms ORDER BY number ASC")
    LiveData<List<Term>> getAllTerms();

}
