package com.example.coursetracker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class AssessmentFlatAdapter extends RecyclerView.Adapter<AssessmentFlatAdapter.AssessmentHolder> {
    private List<Assessment> assessments = new ArrayList<>();
    private OnAssessmentClickListener listener;

    @NonNull
    @Override
    public AssessmentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.assessment_cardview_flat, parent, false);
        return new AssessmentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AssessmentHolder holder, int position) {
        Assessment currentAssessment = assessments.get(position);
        holder.textTitle.setText(currentAssessment.getTitle());
        int score = currentAssessment.getScore();
        String scoreString = "No Score";
        if(score >= 0){
            scoreString = score + "%";
        }
        holder.textScore.setText(scoreString);
        String date = "Due: " + CommonDateFormatter.getInstance().format(currentAssessment.getDueDate());
        holder.textDate.setText(date);
        holder.textType.setText(currentAssessment.getType());
    }

    @Override
    public int getItemCount() {
        return assessments.size();
    }

    public Assessment getAssessmentAt(int index){
        return assessments.get(index);
    }

    public void setAssessments(List<Assessment> assessments){
        this.assessments = assessments;
        notifyDataSetChanged();
    }

    class AssessmentHolder extends RecyclerView.ViewHolder{
        private TextView textTitle;
        private TextView textScore;
        private TextView textDate;
        private TextView textType;

        public AssessmentHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.assessment_title);
            textScore = itemView.findViewById(R.id.assessment_score);
            textDate = itemView.findViewById(R.id.assessment_dates);
            textType = itemView.findViewById(R.id.assessment_type);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int index = getAdapterPosition();
                    if(listener != null && index != RecyclerView.NO_POSITION) {
                        listener.onAssessmentClick(assessments.get(index));
                    }
                }
            });
        }
    }

    public interface OnAssessmentClickListener {
        void onAssessmentClick(Assessment assessment);
    }

    public void setOnAssessmentClickListener(AssessmentFlatAdapter.OnAssessmentClickListener listener){
        this.listener = listener;
    }
}
