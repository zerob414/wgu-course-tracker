package com.example.coursetracker;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class ViewAddEditAssessmentActivity extends AppCompatActivity implements ViewAssessmentFragment.AssessmentActionListener, EditAssessmentFragment.OnSaveAssessmentClickInterface {
    public static final String EXTRA_ID = "com.example.coursetracker.EXTRA_ID";
    public static final String EXTRA_TITLE = "com.example.coursetracker.EXTRA_TITLE";
    public static final String EXTRA_TYPE = "com.example.coursetracker.EXTRA_TYPE";
    public static final String EXTRA_START_DATE = "com.example.coursetracker.EXTRA_START_DATE";
    public static final String EXTRA_DUE_DATE = "com.example.coursetracker.EXTRA_DUE_DATE";
    public static final String EXTRA_NOTES = "com.example.coursetracker.EXTRA_NOTES";
    public static final String EXTRA_SCORE = "com.example.coursetracker.EXTRA_SCORE";
    public static final String EXTRA_COURSE_ID = "com.example.coursetracker.EXTRA_COURSE_ID";

    private AssessmentViewModel assessmentViewModel;
    private Bundle extras;
    private Assessment selectedAssessment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_assessment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        assessmentViewModel = ViewModelProviders.of(this).get(AssessmentViewModel.class);
        Intent intent = getIntent();
        if (intent == null) {
            Toast.makeText(this, "An error occurred, please restart the app", Toast.LENGTH_SHORT).show();
        } else {
            extras = intent.getExtras();
            if (extras == null) {
                extras = new Bundle();
            }
            if (extras.getInt(EXTRA_ID) == 0) {
                switchToEditAssessment(new Assessment());
            } else {
                LiveData<List<Assessment>> allAssessments = assessmentViewModel.getAllAssessments();
                allAssessments.observe(this, assessments -> {
                    for (Assessment assessment : assessments) {
                        if (assessment.getAssessmentId() == extras.getInt(EXTRA_ID)) {
                            selectedAssessment = assessment;
                            switchToViewAssessment(selectedAssessment);
                            break;
                        }
                    }
                });
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Fragment fragment = getFragmentManager().findFragmentByTag(EditAssessmentFragment.TAG);
        if (fragment != null && fragment.isVisible() && selectedAssessment != null) {
            switchToViewAssessment(selectedAssessment);
        } else {
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onAddReminderClick() {
        if (selectedAssessment != null) {
            CharSequence[] options = new CharSequence[]{getApplicationContext().getString(R.string.start_date), getApplicationContext().getString(R.string.due_date)};

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Create a reminder for:");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Date dueDate = selectedAssessment.getStartDate();
                    String verb = "starting";
                    if(which == 1){
                        dueDate = selectedAssessment.getDueDate();
                        verb = "due";
                    }
                    String date = CommonDateFormatter.getInstance().format(dueDate);
                    Calendar calDueDate = new GregorianCalendar();
                    calDueDate.setTime(dueDate);
                    Calendar calDayBefore = new GregorianCalendar();
                    calDayBefore.set(Calendar.YEAR, calDueDate.get(Calendar.YEAR));
                    calDayBefore.set(Calendar.MONTH, calDueDate.get(Calendar.MONTH));
                    calDayBefore.set(Calendar.DAY_OF_MONTH, calDueDate.get(Calendar.DAY_OF_MONTH));
                    calDayBefore.add(Calendar.DAY_OF_MONTH, -1);
                    Intent intent = new Intent(ViewAddEditAssessmentActivity.this, ViewAddEditReminderActivity.class);
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ID, 0);
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_TITLE, selectedAssessment.getTitle() + " " + verb + " soon!");
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_MESSAGE, "The assessment " + selectedAssessment.getTitle() + " will be " + verb + " on " + date + ".");
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ENTITY_ID, selectedAssessment.getAssessmentId());
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ENTITY_TITLE, selectedAssessment.getTitle());
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_ENTITY_TYPE, Reminder.TYPE_ASSESSMENT);
                    intent.putExtra(ViewAddEditReminderActivity.EXTRA_DATE, calDayBefore.getTime().getTime());
                    startActivity(intent);
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //the user clicked on Cancel
                }
            });
            builder.show();
        }
    }

    @Override
    public void share() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, selectedAssessment.getNotes());
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Check out my notes for " + selectedAssessment.getTitle());
        sendIntent.setType("text/plain");
        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }

    @Override
    public void onEditClick() {
        if (extras == null) {
            switchToEditAssessment(new Assessment());
        } else {
            switchToEditAssessment(selectedAssessment);
        }
    }

    @Override
    public void onSaveClick(Assessment assessment) {
        if (assessment != null) {
            selectedAssessment = assessment;
            if (assessment.getAssessmentId() == 0) {
                assessmentViewModel.insert(assessment);
            } else {
                assessmentViewModel.update(assessment);
            }
        }
        switchToViewAssessment(selectedAssessment);
    }

    @Override
    public void onUpClick() {
        onBackPressed();
    }

    @Override
    public void onHomeClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragmentManager().findFragmentByTag(EditAssessmentFragment.TAG);
        if (fragment != null && fragment.isVisible() && selectedAssessment != null) {
            switchToViewAssessment(selectedAssessment);
        } else {
            finish();
        }

    }

    private void switchToEditAssessment(Assessment assessment) {
        EditAssessmentFragment newFragment = new EditAssessmentFragment();
        newFragment.setAssessment(assessment);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment, EditAssessmentFragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void switchToViewAssessment(Assessment assessment) {
        ViewAssessmentFragment newFragment = new ViewAssessmentFragment();
        newFragment.setAssessment(assessment);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment, ViewAssessmentFragment.TAG);
        transaction.commit();
    }

}
