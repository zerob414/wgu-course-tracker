package com.example.coursetracker;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class CourseViewModel extends AndroidViewModel {
    private CourseTrackerRepository repository;
    private LiveData<List<Course>> allCourses;
    private List<Course> allCoursesList;

    public CourseViewModel(@NonNull Application application) {
        super(application);
        repository = new CourseTrackerRepository(application);
        allCourses = repository.getAllCourses();
    }

    public void insert(Course course){
        repository.insert(course);
    }

    public void update(Course course){
        repository.update(course);
    }

    public void delete(Course course){
        repository.delete(course);
    }

    public void deleteAllCourses(){
        repository.deleteAllCourses();
    }

    public LiveData<List<Course>> getAllCourses(){
        return allCourses;
    }

    public List<Course> getAllCoursesList(){
        return allCoursesList;
    }
}
