package com.example.coursetracker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TermActionListener} interface
 * to handle interaction events.
 * Use the {@link ViewTermFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewTermFragment extends Fragment {
    public static final String TAG = "com.example.coursetracker.ViewTermFragment";

    private TextView textTitle;
    private TextView textStartDate;
    private TextView textEndDate;
    private TextView textTermNumber;
    private TextView textNotes;

    private Term selectectTerm;

    private TermActionListener mListener;
    private CourseViewModel courseViewModel;

    public ViewTermFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id    The term's SQLite/Room ID number, null if new Term
     * @param title  The term's title
     * @param startDate The term's start date
     * @param endDate The term's end date
     * @param order The term's number for sorting
     * @param notes The term's notes
     * @return A new instance of fragment ViewTermFragment.
     */
    public static ViewTermFragment newInstance(Integer id, String title, Date startDate, Date endDate, int order, String notes) {
        ViewTermFragment fragment = new ViewTermFragment();
        Bundle args = new Bundle();
        args.putInt(ViewAddEditTermActivity.EXTRA_ID, id);
        args.putString(ViewAddEditTermActivity.EXTRA_TITLE, title);
        args.putLong(ViewAddEditTermActivity.EXTRA_START_DATE, startDate.getTime());
        args.putLong(ViewAddEditTermActivity.EXTRA_END_DATE, endDate.getTime());
        args.putInt(ViewAddEditTermActivity.EXTRA_NUMBER, order);
        args.putString(ViewAddEditTermActivity.EXTRA_NOTES, notes);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        textTitle = getView().findViewById(R.id.textTitle);
        textStartDate = getView().findViewById(R.id.textStartDate);
        textEndDate = getView().findViewById(R.id.textEndDate);
        textTermNumber = getView().findViewById(R.id.textTermNumber);
        textNotes = getView().findViewById(R.id.textNotes);

        getActivity().setTitle("View Term");

        if(selectectTerm != null) {
            textTitle.setText(selectectTerm.getTitle());
            textStartDate.setText(CommonDateFormatter.getInstance().format(selectectTerm.getStart()));
            textEndDate.setText(CommonDateFormatter.getInstance().format(selectectTerm.getEnd()));
            textTermNumber.setText(String.valueOf(selectectTerm.getNumber()));
            textNotes.setText(selectectTerm.getNotes());

            RecyclerView recyclerView = getView().findViewById(R.id.recyclerCourses);
            recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
            recyclerView.setHasFixedSize(true);

            CourseFlatAdapter adapter = new CourseFlatAdapter();
            recyclerView.setAdapter(adapter);

            courseViewModel = ViewModelProviders.of(this).get(CourseViewModel.class);
            courseViewModel.getAllCourses().observe(this, new Observer<List<Course>>() {
                @Override
                public void onChanged(List<Course> courses) {
                    ArrayList<Course> filteredCourseList = new ArrayList<>();
                    for(Course course : courses){
                        if(course.getTermId() == selectectTerm.getTermId()){
                            filteredCourseList.add(course);
                        }
                    }
                    adapter.setCourses(filteredCourseList);
                }
            });
            adapter.setOnCourseClickListener(new CourseFlatAdapter.OnCourseClickListener() {
                @Override
                public void onCourseClick(Course course) {
                    Intent intent = new Intent(ViewTermFragment.this.getActivity(), ViewAddEditCourseActivity.class);
                    intent.putExtra(ViewAddEditCourseActivity.EXTRA_ID, course.getCourseId());
                    startActivity(intent);
                }
            });
        }
    }

    public void setTerm(Term term){
        selectectTerm = term;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menuInflater.inflate(R.menu.view_term_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.add_term_reminder:
                mListener.onAddReminderClick();
                return true;
            case R.id.share:
                mListener.share();
                return true;
            case R.id.edit_term:
                mListener.onEditClick();
                return true;
            case android.R.id.home:
                mListener.onHomeClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_term, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TermActionListener) {
            mListener = (TermActionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TermActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Interface to send events back to the Activity.
     */
    public interface TermActionListener {
        void onAddReminderClick();
        void onEditClick();
        void onHomeClick();
        void share();
    }
}
