package com.example.coursetracker;

import android.content.Context;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonDateFormatter {
    private static final CommonDateFormatter ourInstance = new CommonDateFormatter();
    public static final String DATE_PATTERN = "d MMMM yyyy";
    public static final String TIME_PATTERN_12 = "h:mma";
    public static final String TIME_PATTERN_24 = "k:mm";
    DateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);
    DateFormat time12Formatter = new SimpleDateFormat(TIME_PATTERN_12);
    DateFormat time24Formatter = new SimpleDateFormat(TIME_PATTERN_24);

    public static CommonDateFormatter getInstance() {
        return ourInstance;
    }

    private CommonDateFormatter() {
    }

    /**
     * Formats a date object into a String using the date format common to this app. Returns null
     * if the passed in date object is null.
     * @param date
     * @return
     */
    public String format(Date date){
        if(date == null)return null;
        return dateFormatter.format(date);
    }

    public static Date parseTime(String dateString, Context context){
        try {
            if (android.text.format.DateFormat.is24HourFormat(context)) {
                return new SimpleDateFormat(TIME_PATTERN_24).parse(dateString);
            }
            else{
                return new SimpleDateFormat(TIME_PATTERN_12).parse(dateString);
            }
        }
        catch (ParseException e){
            return null;
        }
    }

    /**
     * Parses a date String from the date format common to this app. Returns null on error.
     * @param dateString
     * @return
     */
    public static Date parse(String dateString){
        try {
            return new SimpleDateFormat(DATE_PATTERN).parse(dateString);
        }
        catch (ParseException e){
            return null;
        }
    }

    public String formatTime(Date date, Context context) {
        if (android.text.format.DateFormat.is24HourFormat(context)) {
            return time24Formatter.format(date);
        }
        else{
            return time12Formatter.format(date);
        }
    }

    public String formatWithTime(Date date, Context context){
        return formatTime(date, context) + ", " + format(date);
    }
}
