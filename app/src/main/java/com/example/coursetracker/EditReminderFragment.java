package com.example.coursetracker;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnSaveReminderClickInterface} interface
 * to handle interaction events.
 * Use the {@link EditReminderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditReminderFragment extends Fragment implements View.OnFocusChangeListener, View.OnClickListener {
    public static final String TAG = "com.example.coursetracker.EditReminderFragment";

    private TextView textEntityTitle;
    private TextView textEntityType;
    private EditText titleText;
    private Button buttonDate;
    private Button buttonTime;
    private EditText messageText;

    private Reminder selectedReminder;
    private Date selectedDateTime;

    private OnSaveReminderClickInterface mListener;

    public EditReminderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id          The reminder's SQLite/Room ID number
     * @param title       The reminder's title
     * @param date        The reminder's date
     * @param entityType  The reminder's type
     * @param entityId    The ID of the entity this reminder is for
     * @param entityTitle The title of the entity this reminder is for
     * @return A new instance of fragment EditReminderFragment.
     */
    public static EditReminderFragment newInstance(int id, String title, Date date, String entityType, int entityId, String entityTitle, String message) {
        EditReminderFragment fragment = new EditReminderFragment();
        Bundle args = new Bundle();
        args.putInt(ViewAddEditReminderActivity.EXTRA_ID, id);
        args.putString(ViewAddEditReminderActivity.EXTRA_TITLE, title);
        args.putString(ViewAddEditReminderActivity.EXTRA_ENTITY_TYPE, entityType);
        args.putString(ViewAddEditReminderActivity.EXTRA_ENTITY_TITLE, entityTitle);
        args.putLong(ViewAddEditReminderActivity.EXTRA_DATE, date.getTime());
        args.putInt(ViewAddEditReminderActivity.EXTRA_ENTITY_ID, entityId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onClick(View view) {
        if(selectedDateTime == null) selectedDateTime = new Date();
        if(view == buttonDate){
            Date reminderDate = CommonDateFormatter.parse(buttonDate.getText().toString());
            if(reminderDate == null){
                reminderDate = new Date();
            }
            Calendar cal = new GregorianCalendar();
            cal.setTime(reminderDate);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar cal = new GregorianCalendar();
                    cal.setTime(selectedDateTime);
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, monthOfYear);
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    selectedDateTime = cal.getTime();
                    buttonDate.setText(CommonDateFormatter.getInstance().format(selectedDateTime));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
        else if(view == buttonTime){
            Date reminderDate = CommonDateFormatter.parseTime(buttonTime.getText().toString(), getContext());
            if(reminderDate == null){
                reminderDate = new Date();
            }
            Calendar cal = new GregorianCalendar();
            cal.setTime(reminderDate);
            TimePickerDialog datePickerDialog = new TimePickerDialog(this.getContext(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                    Calendar cal = new GregorianCalendar();
                    cal.setTime(selectedDateTime);
                    cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    cal.set(Calendar.MINUTE, minute);
                    selectedDateTime = cal.getTime();
                    buttonTime.setText(CommonDateFormatter.getInstance().formatTime(selectedDateTime, getContext()));
                }
            }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), android.text.format.DateFormat.is24HourFormat(getContext()));
            datePickerDialog.show();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        titleText = getView().findViewById(R.id.titleText);
        buttonDate = getView().findViewById(R.id.buttonDate);
        buttonTime = getView().findViewById(R.id.buttonTime);
        textEntityType = getView().findViewById(R.id.textEntityType);
        textEntityTitle = getView().findViewById(R.id.textEntityName);
        messageText = getView().findViewById(R.id.messageText);

        titleText.setOnFocusChangeListener(this);
        messageText.setOnFocusChangeListener(this);

        buttonDate.setOnClickListener(this);
        buttonTime.setOnClickListener(this);

        getActivity().setTitle("Add Reminder");
        if(selectedReminder != null) {
            if(selectedReminder.getReminderId() > 0){
                getActivity().setTitle("Edit Reminder");
            }
            titleText.setText(selectedReminder.getTitle());
            selectedDateTime = selectedReminder.getDate();
            textEntityType.setText(selectedReminder.getEntityType() + ":");
            textEntityTitle.setText(selectedReminder.getEntityTitle());
            messageText.setText(selectedReminder.getMessage());
        }
        else{
            selectedDateTime = new Date();
        }
        buttonDate.setText(CommonDateFormatter.getInstance().format(selectedDateTime));
        buttonTime.setText(CommonDateFormatter.getInstance().formatTime(selectedDateTime, getContext()));
    }

    public void setReminder(Reminder reminder){
        selectedReminder = reminder;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menuInflater.inflate(R.menu.edit_reminder_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.save_reminder:
                saveReminder();
                return true;
            case android.R.id.home:
                mListener.onUpClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveReminder(){
        if(titleText.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please enter a title", Toast.LENGTH_SHORT).show();
        }
        else if(buttonDate.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please select a date", Toast.LENGTH_SHORT).show();
        }
        else if(buttonTime.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please select a time", Toast.LENGTH_SHORT).show();
        }
        else if(messageText.getText().toString().trim().isEmpty()){
            Toast.makeText(getContext(), "Please enter a message", Toast.LENGTH_SHORT).show();
        }
        else {
            if(selectedReminder == null)selectedReminder = new Reminder();
            if(selectedDateTime == null)selectedDateTime = new Date();
            selectedReminder.setTitle(titleText.getText().toString().trim());
            selectedReminder.setEntityType(textEntityType.getText().toString().replace(":", ""));
            selectedReminder.setEntityTitle(textEntityTitle.getText().toString());
            selectedReminder.setMessage(messageText.getText().toString().trim());
            Date date = CommonDateFormatter.parse(buttonDate.getText().toString());
            Date time = CommonDateFormatter.parseTime(buttonTime.getText().toString(), getContext());
            Calendar dateTime = new GregorianCalendar();
            dateTime.setTime(date);
            Calendar timeCal = new GregorianCalendar();
            timeCal.setTime(time);
            dateTime.set(Calendar.HOUR, timeCal.get(Calendar.HOUR));
            dateTime.set(Calendar.MINUTE, timeCal.get(Calendar.MINUTE));
            dateTime.set(Calendar.SECOND, timeCal.get(Calendar.SECOND));
            selectedReminder.setDate(selectedDateTime);
            mListener.onSaveClick(selectedReminder);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_reminder, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSaveReminderClickInterface) {
            mListener = (OnSaveReminderClickInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEditReminderClickInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(view instanceof EditText && !hasFocus){
            InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Interface to send events back to the Activity.
     */
    public interface OnSaveReminderClickInterface {
        void onSaveClick(Reminder reminder);
        void onUpClick();
    }
}
