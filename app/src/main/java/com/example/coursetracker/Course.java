package com.example.coursetracker;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Entity(tableName = "tbl_courses")
public class Course {
    @Ignore
    public static final String PLANNED = "Planned";
    @Ignore
    public static final String IN_PROGRESS = "In Progress";
    @Ignore
    public static final String PASSED = "Passed";
    @Ignore
    public static final String FAILED = "Failed";
    @Ignore
    public static final String DROPPED = "Dropped";

    //To be displayed in the status spinner on EditCourseFragment.
    @Ignore
    public static final String[] STATUSES_ARRAY = {PLANNED, IN_PROGRESS, PASSED, FAILED, DROPPED};

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "course_id")
    private int courseId;
    private String title;
    private String status;
    @ColumnInfo(name = "start_date")
    private Date startDate;
    @ColumnInfo(name = "end_date")
    private Date endDate;
    private String notes;
    @ColumnInfo(name = "mentor_id")
    private int mentorId;
    @ColumnInfo(name = "term_id")
    private int termId;

    public Course(String title, String status, Date startDate, Date endDate, String notes, Integer mentorId, Integer termId) {
        this.title = title;
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
        this.notes = notes;
        this.mentorId = mentorId;
        this.termId = termId;
    }

    @Ignore
    public Course(){
        startDate = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(startDate);
        cal.add(Calendar.MONTH, 6);
        endDate = cal.getTime();
    }

    public int getTermId() {
        return termId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getTitle() {
        return title;
    }

    public String getStatus() {
        return status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getNotes() {
        return notes;
    }

    public Date getEndDate() { return endDate; }

    public int getMentorId() { return mentorId; }

    public int getCourseId() { return courseId; }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setMentorId(int mentorId) {
        this.mentorId = mentorId;
    }

    public void setTermId(int termId) {
        this.termId = termId;
    }

    @NonNull
    @Override
    public String toString() {
        return title;
    }
}
