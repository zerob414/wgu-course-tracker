package com.example.coursetracker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.CourseHolder> {
    private List<Course> courses = new ArrayList<>();
    private OnCourseClickListener listener;

    @NonNull
    @Override
    public CourseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.course_cardview, parent, false);
        return new CourseHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseHolder holder, int position) {
        Course currentCourse = courses.get(position);
        holder.textTitle.setText(currentCourse.getTitle());
        String term = currentCourse.getTermId() > 0 ? String.valueOf(currentCourse.getTermId()) : "N/A";
        holder.textTerm.setText(term);
        String date = "Start Date: " + CommonDateFormatter.getInstance().format(currentCourse.getStartDate());
        holder.textStartDate.setText(date);
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public Course getCourseAt(int index){
        return courses.get(index);
    }

    public void setCourses(List<Course> courses){
        this.courses = courses;
        notifyDataSetChanged();
    }

    class CourseHolder extends RecyclerView.ViewHolder{
        private TextView textTitle;
        private TextView textTerm;
        private TextView textStartDate;

        public CourseHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.course_title);
            textTerm = itemView.findViewById(R.id.course_term);
            textStartDate = itemView.findViewById(R.id.course_start);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int index = getAdapterPosition();
                    if(listener != null && index != RecyclerView.NO_POSITION) {
                        listener.onCourseClick(courses.get(index));
                    }
                }
            });
        }
    }

    public interface OnCourseClickListener {
        void onCourseClick(Course course);
    }

    public void setOnCourseClickListener(CourseAdapter.OnCourseClickListener listener){
        this.listener = listener;
    }
}
