package com.example.coursetracker;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.Calendar;
import java.util.Date;

@Database(entities = {Term.class, Course.class, Assessment.class, Mentor.class, Reminder.class}, version = 16)
@TypeConverters({Converters.class})
public abstract class CourseTrackerDatabase extends RoomDatabase {
    private static CourseTrackerDatabase ourInstance;
    public abstract TermDao termDao();
    public abstract CourseDao courseDao();
    public abstract AssessmentDao assessmentDao();
    public abstract MentorDao mentorDao();
    public abstract ReminderDao reminderDao();

    public static synchronized CourseTrackerDatabase getInstance(Context context) {
        if(ourInstance == null){
            ourInstance = Room.databaseBuilder(context.getApplicationContext(), CourseTrackerDatabase.class, "course_tracker.db").fallbackToDestructiveMigration().addCallback(roomCallback).build();
        }
        return ourInstance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new AddSampleTermsAsyncTask(ourInstance).execute();
        }
    };

    private static class AddSampleTermsAsyncTask extends AsyncTask<Void, Void, Void>{
        private TermDao termDao;

        private AddSampleTermsAsyncTask(CourseTrackerDatabase db){
            this.termDao = db.termDao();
        }

        @Override
        protected Void doInBackground(Void... voids){
            final int SAMPLE_TERMS_MAX = 8;
            Calendar c = Calendar.getInstance();
            for(int i = 1; i < SAMPLE_TERMS_MAX; i++){
                Date start = c.getTime();
                c.add(Calendar.MONTH, 6);
                Date end = c.getTime();
                termDao.insert(new Term("Term " + i, start ,end, i, ""));
            }
            return null;
        }
    }
}
