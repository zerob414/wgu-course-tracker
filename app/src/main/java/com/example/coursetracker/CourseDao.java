package com.example.coursetracker;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CourseDao {
    @Insert
    long insert(Course course);

    @Update
    void update(Course course);

    @Delete
    void delete(Course course);

    @Query("DELETE FROM tbl_courses")
    void deleteAllCourses();

    @Query("SELECT * FROM tbl_courses ORDER BY title ASC")
    LiveData<List<Course>> getAllCourses();

}
