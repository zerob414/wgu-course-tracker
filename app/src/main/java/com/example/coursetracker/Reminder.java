package com.example.coursetracker;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Date;

@Entity(tableName = "tbl_reminders")
public class Reminder {
    @Ignore
    public static final String TYPE_ASSESSMENT = "Assessment";
    @Ignore
    public static final String TYPE_COURSE = "Course";
    @Ignore
    public static final String TYPE_TERM = "Term";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "reminder_id")
    private int reminderId;
    private String title;
    private String message;
    @ColumnInfo(name = "entity_id")
    private int entityId;
    @ColumnInfo(name = "entity_type")
    private String entityType;
    private Date date;
    @Ignore
    private String entityTitle;

    @Ignore
    ArrayList<ReminderIdUpdatedListener> listeners;

    public Reminder(String title, String message, Date date) {
        this.title = title;
        this.message = message;
        this.date = date;
    }

    @Ignore
    public Reminder(){}

    public int getReminderId() {
        return reminderId;
    }

    public void setReminderId(int reminderId) {
        this.reminderId = reminderId;
        if(listeners != null) {
            for (ReminderIdUpdatedListener listener : listeners) {
                listener.reminderIdUpdated(this);
            }
        }
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityTitle() {
        return entityTitle;
    }

    public void setEntityTitle(String entityTitle) {
        this.entityTitle = entityTitle;
    }

    @NonNull
    @Override
    public String toString() {
        return title;
    }

    public void addIdUpdatedListener(ReminderIdUpdatedListener listener){
        if(listeners == null)listeners = new ArrayList<>();
        if(!listeners.contains(listener))listeners.add(listener);
    }

    public void removeIdUpdatedListener(ReminderIdUpdatedListener listener){
        if(listeners != null)listeners.remove(listener);
    }

    interface ReminderIdUpdatedListener{
        void reminderIdUpdated(Reminder reminder);
    }
}
