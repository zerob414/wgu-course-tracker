# Course Tracker
Project for C196 Mobile Application Development course at Western
Governors University.

This application was developed for a minimum SDK of 26 with a target of
SDK 29. IT was built and tested on the Pixel 2 emulator running API
version 28.
  
## Mobile Application Development
Mobile application development differs greatly from desktop or web
application development. Users have come to expect highly dynamic and
responsive applications with intuitive workflows and minimal effort for
accomplishing tasks. Additionally, mobile applications have evolved to
have multiple entry points for directly handling data passed in from
other applications. For example, a user may press the "Share" button in
an app displaying an image. The application they select to share to
image to might be a social media app that will add the image to the
user's timeline with a message. The social media app bypasses the main
user interface and directly opens the interface to edit a new message to
be posted on the user's timeline along with the image. Using `intent`
objects in Android makes this a very standardized and easy workflow that
is fairly intuitive to the user. There are no such equivalent frameworks
for desktop or web applications that provide the robust structured and
standardized data passing capability Android does. 

Hardware capabilities are drastically different as well, despite the
fact that mobile phones nowadays are more powerful (in terms of
processing power, storage and graphics) than the desktop computers many
people of my generation first used as children. Mobile phones these days
have gyroscopic sensors, accelerometers, magnetometers, barometers,
thermometers, GPS receivers, high definition cameras, high definition
touchscreens, several radios, and much much more. These open up amazing
interactive capabilities that add volumes of value to consumers.

A downside is that these devices are still relatively underpowered
compared to desktop machines. It's easy to forget that until developers
need strong encryption algorithms, large data analysis, or even good
looking 3D graphics. As powerful as the tiny computers are, they still
have limitations. For example, the RSA encryption algorithm is widely
used for securing data-in-transit. At high bit lengths it is considered
very secure, however, low end mobile devices often do not have the power
to process encryption at such levels without causing delays that user's
may find annoying or frustrating. Even high end devices may experience
delays if the user has multiple tasks going on at the same time.

## Challenges

### MVVM vs MVC
The first challenge I faced was changing the way I thought about
interface design. My background has been all MVC (model, view,
controller) from Java Swing, JavaFX, and some web development with PHP
frameworks. While the differences between the two design techniques are
not grand, it did take me quite a bit of time and experimentation to
transition my mindset before I figured out how to make a working
application template. I overcame this challenge by doing a lot of
reading online, following many tutorials and by trial-and-error which
resulted in creating and deleting many classes. Once I figured it out,
implementing application features became an easier and quicker process.

### View and Edit Fragments
My initial design included separate activities for viewing and editing
each of the database entities and used intents to pass information
between them. It was cumbersome, the usability felt clunky and it just
didn't seem to flow how I wanted. To solve the problem, I researched and
learned about _fragments_. I did some trial-and-error experimentation
and eventually came up with a model for using two fragments in a single
activity that would operate on the same database entity, therefore
eliminating some code duplication. I replaced my two-activity solution
with the two-fragment-one-activity solution and was able to achieve the
smoother workflows I was looking for.

### Scheduling Notifications
Scheduling notifications was a large challenge because the techniques
for doing so have changed drastically several times throughout Android's
history. I spent several days without writing any code just trying to
figure out what the most reliable way to do it on modern Android is. I
eventually came across an article called _Scheduling Notifications on
Android with WorkManager_ by Adrian Tache. This article solved my
problem and became the basis for the solution I implemented.

## Redux
If I had to do this project over again, there are two things I would do
differently.

Firstly, I would make full use of foreign key constraints with SQLite
and Room. Currently, there are no foreign key constraints due to an
issue with actually getting them to work with Room. I was probably
missing something, but after hours of reading developer pages and
scouring StackOverflow, Room still would not accept my foreign key
constraints as valid, despite me being 99% certain they were correct.
With more time, I would ask a more experienced developer to help me
figure out what I was doing wrong.

Secondly, I would build the app with a more standard workflow utilizing
a navigation drawer rather than buttons on the main activity. Now that I
am much more familiar with fragments, I now understand how I would make
that work. It was too late to rearchitect my project once I learned how
to use fragments this time around.

## Application Workflow
Below is a visual layout of the workflow for the app. 

![Workflow Diagram](diagrams/activity_workflow.png)

## Emulators for Development
Developing on Android using Android Studio typically involves using the
Android "emulators" for testing. I put the word "emulators" in quotes
because these are in fact full versions of the Android operating system,
tweaked for debugging and development, running in virtual machines (KVM
in my case on Linux). The emulator does provide an extensive amount of
features useful for testing and debugging, the most valuable of which is
probably the piping of the output streams (standard out (stdout) and
standard error (stderr)) to Android Studio's "Logcat" tab. This feature,
however, is also available on physical devices connected via USB and
operating in _developer mode_. On the emulator developers can simulate
GPS locations and movements, device rotation, network connections, and
much more. This allows rapid development and testing without requiring
the developer to physically test every feature. It also allows
comparisons using exactly the same input data to tweak for expected
results.

On the other hand, using physical test devices provides more realistic
results. For example, when testing a computationally intensive tast, the
emulator might not throttle the virtual CPU low enough to realistically
simulate a real device. Additionally, the emulator is likely cleaner
than a real device, void of other apps and services users have
downloaded and are running. Real devices may have quirks that arise from
vendor customization of the operating system that don't exist in the
emulator. Emulators can only go so far in providing feedback to
developers on user experience, at some point the app needs to be tested
"in the wild".

## References
Tache, A. (2019, May 8). Scheduling Notifications on Android with
WorkManager. Retrieved September 24, 2019, from
[https://medium.com/android-ideas/scheduling-notifications-on-android-with-workmanager-6d84b7f64613.]